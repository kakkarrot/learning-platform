package client;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;

import controller.CoursePanelListener;
import controller.HomeListener;
import controller.LoginListener;
import shareddataclasses.Assignment;
import shareddataclasses.Course;
import shareddataclasses.Grade;
import shareddataclasses.InfoHolder;
import shareddataclasses.LoginInfo;
import shareddataclasses.Message;
import shareddataclasses.Student;
import shareddataclasses.Submission;
import view.LoginGui;
import view.ProfessorGui;
import view.StudentGui;
import view.View;

/**
 * Client is the class that tries to open a socket connection with the server.
 * 
 * @author Erik Skoronski
 * @author Qifeng Li
 *
 */
public class Client {
	/**
	 * The Socket the client will write to
	 */
	private Socket socket;
	
	/**
	 * The ObjectOutputStream Client will use
	 */
	private ObjectOutputStream socketOut;
	
	/**
	 * The ObjectInputStream Client will use
	 */
	private ObjectInputStream socketIn;
	
	/**
	 * The gui for the Client
	 */
	private View view;
	
	/**
	 * The userid of the person using view
	 */
	private String userid;
	
	/**
	 * True if userid belongs to a student
	 */
	private boolean student;

	/**
	 * Main constructor for Client
	 * @param serverName The server name. 
	 * @param portNumber The port number. 
	 */
	public Client(String serverName, int portNumber) {
		try {
			socket = new Socket(serverName, portNumber);

			socketOut = new ObjectOutputStream(socket.getOutputStream());

			socketIn = new ObjectInputStream(socket.getInputStream());
		} catch (IOException e) {
			System.err.println(e.getStackTrace());
		}
	}

	/**
	 * This method reads a line from the user and prints it to the socket that the
	 * client has connected to.
	 * @throws ClassNotFoundException 
	 */
	public void communicate() {
		login();
	}

	/**
	 * Takes a Message and analyzes the contents based on the string command. 
	 * @param m The Message. 
	 */
	public void analyzeMessage(Message<?> m) {
		analyzeMessageLogin(m);
		if(m.getCommand().equals("createCourse"))
		{
			((ProfessorGui) view).createCourseList((Vector<Course>) m.getContent());
		}
		else if(m.getCommand().equals("courseCreated"))
		{
			((ProfessorGui) view).createCourseList((Vector<Course>) m.getContent());
		}
		else if(m.getCommand().equals("idSearch"))
		{
			view.getSearchResultsPanel().getListModel().clear();
			view.getSearchResultsPanel().getListModel().addElement(m.getContent());
		}
		else if(m.getCommand().equals("lastNameSearch"))
		{
			System.out.println("Searching by lastname in client");
			view.getSearchResultsPanel().getListModel().clear();
			view.getSearchResultsPanel().getListModel().addElement(m.getContent());
		}
		else if(m.getCommand().equals("courseToAssignment"))
		{
			view.getAssignmentPanel().getListModel().clear();
			Vector<Assignment> v = (Vector<Assignment>) m.getContent();
			for(int i = 0; i < v.size(); i++)
				view.getAssignmentPanel().getListModel().addElement
					((v.elementAt(i).getAssignment_id()) + " " + (v.elementAt(i)).getTitle());
		}
		else if(m.getCommand().equals("addNewAssignment"))
		{
			view.getAssignmentPanel().getListModel().clear();
			view.getAssignmentPanel().getListModel().addElement(((Assignment) m.getContent()).getTitle());
		}
		else if(m.getCommand().equals("assignmentToDropbox")||m.getCommand().equals("uploadSubmission"))
		{
			view.getDropboxPanel().getListModel().clear();
			Vector<Submission> v = (Vector<Submission>) m.getContent();
			for(int i = 0; i < v.size(); i++)
				view.getDropboxPanel().getListModel().addElement
					((v.elementAt(i)).getSubmissionid() + " " + (v.elementAt(i)).getTitle());
		}
		else if(m.getCommand().equals("downloadAssignment"))
		{
			//this is where it actually saves the assignment to the client side
			byte[] content = ((Assignment) m.getContent()).getDataStream();
			// 4) Save the received file the same way it was read:
			File newFile = new File("C:\\Users\\lqf54\\Downloads\\" + ((Assignment) m.getContent()).getTitle() + ".txt");
			try {
				if (!newFile.exists())
					newFile.createNewFile();
				FileOutputStream writer = new FileOutputStream(newFile);
				BufferedOutputStream bos = new BufferedOutputStream(writer);
				bos.write(content);
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if(m.getCommand().equals("downloadSubmission"))
		{
			byte[] content = ((Submission) m.getContent()).getDataStream();
			File newFile = new File("C:\\Users\\lqf54\\Downloads\\" + ((Submission) m.getContent()).getTitle() + ".txt");
			try {
				if (!newFile.exists())
					newFile.createNewFile();
				FileOutputStream writer = new FileOutputStream(newFile);
				BufferedOutputStream bos = new BufferedOutputStream(writer);
				bos.write(content);
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		else if(m.getCommand().equals("courseToGrades"))
		{
			view.getGradesPanel().getListModel().clear();
			Vector<Grade> v = (Vector<Grade>) m.getContent();
			for(int i = 0; i < v.size(); i++)
				view.getGradesPanel().getListModel().addElement
					((v.elementAt(i)).getAssignment_id() + " " + (v.elementAt(i).getGrade()));
		}

	}
	
	/**
	 * Analyzes the Message if the message is from the LoginGui. 
	 * @param m The Message. 
	 */
	private void analyzeMessageLogin(Message<?> m)
	{
		if (m.getCommand().equals("professorHome"))
		{
			student = false;
			view = new ProfessorGui((Message<Vector<Course>>) m, this);
			view.displayHome();
		}
		else if (m.getCommand().equals("studentHome"))
		{
			student = true;
			view = new StudentGui((Message<Vector<Course>>) m, this);
			view.displayHome();
		}
	}
	
	/**
	 * Creates the login screen. 
	 */
	private void login() {
		LoginGui log = new LoginGui();
		LoginListener l = new LoginListener(log, this);
		log.getLogin().addActionListener(l);
	}
	
	/**
	 * Writes into the socket, then reads the result. 
	 * @param m The Message written into the socket. 
	 * @return The Message read from the socket. 
	 */
	public Message<?> readSockets(Message<?> m)
	{
		try {
			getSocketOut().writeObject(m);
			getSocketOut().flush();
			return (Message<?>) getSocketIn().readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	//below are the getter methods
	public ObjectInputStream getSocketIn()
	{
		return socketIn;
	}
	
	public ObjectOutputStream getSocketOut()
	{
		return socketOut;
	}
	
	public boolean isStudent()
	{
		return student;
	}
	
	public View getView()
	{
		return view;
	}
	
	public String getUserid()
	{
		return userid;
	}
	
	public void setUserid(String s)
	{
		userid = s;
	}

	/**
	 * Run the client.
	 * 
	 * @param args
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		System.out.println("Connecting client to server");
		Client aClient = new Client("localhost", 9091);
		System.out.println("Client connection created");
		aClient.communicate();
	}

}