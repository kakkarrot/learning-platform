package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import client.Client;
import shareddataclasses.LoginInfo;
import shareddataclasses.Message;
import view.CoursePanel;
import view.SearchStudentPanel;

/**
 * An ActionListener for the SearchStudentPanel
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class SearchStudentPanelListener implements ActionListener {
	
	/**
	 * The SearchStudentPanel
	 */
	private SearchStudentPanel panel;
	
	/**
	 * The Client using the SearchStudentPanel
	 */
	private Client client;
	
	/**
	 * Creates a SearchStudentPanel
	 * @param p The SearchStudentPanel
	 * @param c The Client
	 */
	SearchStudentPanelListener(SearchStudentPanel p, Client c)
	{
		panel = p;
		client = c;
	}
	
	/**
	 * Performs different actions based on the action happening within the SearchStudentPanel
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == panel.getSearchButton())
		{
			searchForStudents();
		}
		else if(e.getSource() == panel.getBackButton())
		{
			client.getView().displayHome();
		}
		else if(e.getSource() == panel.getEnrollButton())
		{
			toggleEnrollment("enroll");
		}
		else if(e.getSource() == panel.getUnenrollButton())
		{
			toggleEnrollment("unenroll");
		}
	}
	
	/**
	 * A method  used to process enrollment and unenrollment buttons in the SearchStudentPanel
	 * @param mode
	 */
	public void toggleEnrollment(String mode) {
		if(panel.getJList().isSelectionEmpty())
		{
			//TODO implement popup
			panel.showInvalidEnrollment();
			return;
		}
		else if(mode.equals("enroll"))
		{
			enrollStudent();
		}
		else if(mode.equals("unenroll"))
		{
			unenrollStudent();
		}
	}
	
	/**
	 * A method  used to process enrollment button in the SearchStudentPanel
	 */
	private void enrollStudent() {
		System.out.println("Enroll Student");
		String[] result = (panel.getJList().getSelectedValue()).toString().split(" ");
		Message m = new Message<String>(result[0] + " " + panel.getCourseid(), "enrollStudent");
		client.analyzeMessage(client.readSockets(m));
	}

	/**
	 * A method  used to process unenrollment button in the SearchStudentPanel
	 */
	private void unenrollStudent() {
		System.out.println("Unenroll Student");
		String[] result = (panel.getJList().getSelectedValue()).toString().split(" ");
		Message m = new Message<String>(result[0] + " " + panel.getCourseid(), "unenrollStudent");
		client.analyzeMessage(client.readSockets(m));
	}

	/**
	 * A method  used to process search button in the SearchStudentPanel
	 */
	private void searchForStudents() {
		String query = panel.getQueryField().getText();
		if (panel.getIDRadioButton().isSelected())
		{
			Message m = new Message<String>(query, "idSearch");
			client.analyzeMessage(client.readSockets(m));
		}
		else if(panel.getLastNameRadioButton().isSelected())
		{
			Message m = new Message<String>(query, "lastNameSearch");
			client.analyzeMessage(client.readSockets(m));
		}
		else
		{
			panel.showInvalidSearch();
		}
		
	}

	
}
