package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JFileChooser;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import client.Client;
import shareddataclasses.InfoHolder;
import shareddataclasses.Message;
import view.AssignmentPanel;

/**
 * The listener for the panel that displays assignments
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class AssignmentPanelListener implements ActionListener {

	private AssignmentPanel assPanel;
	private Client client;

	/**
	 * Creates a new AssignmentPanel Listener
	 * @param ass
	 * @param c
	 */
	public AssignmentPanelListener(AssignmentPanel ass, Client c) {
		assPanel = ass;
		client = c;
	}

	/**
	 * Does a certain action depending on which part of the panel the user interacts with.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		activateDeactivateButtons(e);
		if (e.getSource() == assPanel.getUploadButton()) {
			client.getView().displayAddAssignmentPanel(assPanel.getCourseid());
			client.getView().getAddAssignmentPanel()
					.addListener(new AddAssignmentPanelListener(client.getView().getAddAssignmentPanel(), client));
		} else if (e.getSource() == assPanel.getDownloadButton()) 
		{
			if (assPanel.getList().isSelectionEmpty())
			{
				assPanel.showInvalid();
				return;
			}
			String[] temp = ((String) assPanel.getList().getSelectedValue()).split(" ");
			client.analyzeMessage(client.readSockets
					(new Message<String>(temp[0], "downloadAssignment")));
			client.getView().getAssignmentPanel().showDownloaded();
		} 
		else if (e.getSource() == assPanel.getDropboxButton()) {
			if (assPanel.getList().isSelectionEmpty())
			{
				assPanel.showInvalid();
				return;
			}
			String[] temp = ((String)assPanel.getList().getSelectedValue()).split(" ");
			client.getView().displayDropboxPanel(temp[0]); //temporary id needs to be fixed
			client.getView().getDropboxPanel().addListener
				(new DropboxPanelListener(client.getView().getDropboxPanel(), client));
			if (client.isStudent())
			{
				InfoHolder h = new InfoHolder(client.getView().getDropboxPanel().getAssignmentid(), client.getUserid());
				client.getView().getDropboxPanel().changeToStudent();
				client.analyzeMessage(client.readSockets
						(new Message<InfoHolder>(h, "assignmentToDropboxStudent")));
			}
			else
			{
				client.getView().getDropboxPanel().changeToProfessor();
				client.analyzeMessage(client.readSockets
						(new Message<> (client.getView().getDropboxPanel().getAssignmentid(), "assignmentToDropboxProfessor")));
			}
			
		} else if (e.getSource() == assPanel.getBackButton()) {
			client.getView().displayHome();
		}

	}
	
	/**
	 * Toggles assignment activations.
	 * @param e
	 */
	private void activateDeactivateButtons(ActionEvent e)
	{
		if (e.getSource() == assPanel.getDeactiveButton()) {
			if (assPanel.getList().isSelectionEmpty())
			{
				assPanel.showInvalid();
				return;
			}
			String[] temp = ((String) assPanel.getList().getSelectedValue()).split(" ");
			client.analyzeMessage(client.readSockets
					(new Message<String>(temp[0], "assignmentDeactivate")));
			assPanel.showActiveStatus("deactivated");
		} else if (e.getSource() == assPanel.getActiveButton()) {
			if (assPanel.getList().isSelectionEmpty())
			{
				assPanel.showInvalid();
				return;
			}
			String[] temp = ((String) assPanel.getList().getSelectedValue()).split(" ");
			client.analyzeMessage(client.readSockets
					(new Message<String>(temp[0], "assignmentActivate")));
			assPanel.showActiveStatus("activated");
		}
	}

}
