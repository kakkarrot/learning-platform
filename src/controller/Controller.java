package controller;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.Vector;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;

import model.Model;
import shareddataclasses.*;
import view.View;

/**
 * The Controller for the LearningPlatformMVC
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Controller implements Runnable {

	/**
	 * The Model the controller is connected to
	 */
	Model model;
	
	/**
	 * The View the controller is connected to
	 */
	View view;
	
	/**
	 * The ObjectInputStream the controller will read from
	 */
	ObjectInputStream in;
	
	/**
	 * The ObjectOutputStream the controller will write to
	 */
	ObjectOutputStream out;

	/**
	 * The LoginListener that is connected to the view
	 */
	LoginListener loginLis;

	/**
	 * Creates a Controller
	 * @param i The ObjectInputStream
	 * @param o The ObjectOutputStream
	 */
	public Controller(ObjectInputStream i, ObjectOutputStream o) {
		in = i;
		out = o;
		model = new Model();
		view = new View();
	}

	/**
	 * The runnable method used by the threadpool in the server
	 */
	@Override
	public void run() {

		Message messageIn, messageOut;
		while (true) {
			try {
				System.out.println("Waiting for message");
				messageIn = (Message) in.readObject();
				System.out.println("Message received");
				messageOut = analyzeMessage(messageIn);
				out.writeObject(messageOut);
				out.flush();

				System.out.println("Message sent");
			} catch (IOException | ClassNotFoundException e) {
				System.err.println("Error in run");
				break;
			}

		}
	}

	/**
	 * Performs different tasks based on the command of the Message recieved
	 * @param m The Message
	 * @return The Message that results from processing m
	 */
	private Message analyzeMessage(Message m) {
		if (m.getCommand().equals("login")) {
			if (model.checkLoginInfo((LoginInfo) m.getContent())) {
				return createHomeMessage((LoginInfo) m.getContent());
			}
			return new Message<>(null, "invalidLogin");
		} else if (m.getCommand().equals("createCourse")) {
			return new Message<Vector<Course>>(createCourse(m), "courseCreated");
		} else if (m.getCommand().equals("idSearch")) {
			return new Message<Student>(idSearch(m), "idSearch");
		} else if (m.getCommand().equals("lastNameSearch")) {
			return new Message<Student>(lastNameSearch(m), "lastNameSearch");
		} else if (m.getCommand().equals("activate")) {
			model.get_dbManager().setCourseActive(1, (String) m.getContent());
			return new Message<>(null, "activate");
		} else if (m.getCommand().equals("deactivate")) {
			model.get_dbManager().setCourseActive(0, (String) m.getContent());
			return new Message<>(null, "deactivate");
		}

		else if (m.getCommand().equals("enrollStudent")) {
			String[] content = ((String) m.getContent()).split(" ");
			if(!model.get_dbManager().isEnrolled(content[0], content[1]))
				model.get_dbManager().addToEnrollment(content[0], content[1]);
			return new Message<>(null, "enrollStudent");
		}

		else if (m.getCommand().equals("unenrollStudent")) {
			String[] content = ((String) m.getContent()).split(" ");
			if(model.get_dbManager().isEnrolled(content[0], content[1]))
				model.get_dbManager().removeFromEnrollment(content[0], content[1]);
			return new Message<>(null, "unenrollStudent");
		}
		else if (m.getCommand().equals("courseToAssignmentProfessor"))
		{
			return new Message<Vector<Assignment>>
				(model.get_dbManager().getAssignmentProfessor((String) m.getContent()), "courseToAssignment");
		}
		else if(m.getCommand().equals("courseToAssignmentStudent"))
		{
			return new Message<Vector<Assignment>>
				(model.get_dbManager().getAssignmentStudent((String) m.getContent()), "courseToAssignment");
		}
		else if (m.getCommand().equals("uploadAssignment")) {
			Assignment a = (Assignment)m.getContent();
			byte[] content = a.getDataStream();
			// 4) Save the received file the same way it was read:
			File newFile = new File("C:\\Users\\lqf54\\Documents\\LearningPlatformFiles\\" + a.getTitle() + ".txt");
			System.out.println(newFile.getAbsolutePath()); //checking to see where it saves
			try {
				if (!newFile.exists())
					newFile.createNewFile();
				FileOutputStream writer = new FileOutputStream(newFile);
				BufferedOutputStream bos = new BufferedOutputStream(writer);
				bos.write(content);
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			return new Message<Vector<Assignment>>
				(model.get_dbManager().getAssignmentProfessor((String) m.getContent()), "uploadAssignment");
		} 
		else if (m.getCommand().equals("uploadSubmission")) {
			Submission s = (Submission) m.getContent();
			byte[] content = s.getDataStream();
			// 4) Save the received file the same way it was read:
			File newFile = new File("C:\\Users\\lqf54\\Documents\\LearningPlatformUploads\\" + s.getTitle() + ".txt");
			try {
				if (!newFile.exists())
					newFile.createNewFile();
				FileOutputStream writer = new FileOutputStream(newFile);
				BufferedOutputStream bos = new BufferedOutputStream(writer);
				bos.write(content);
				bos.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			model.get_dbManager().addSubmission(s);
			return new Message<Vector<Submission>>
			(model.get_dbManager().getSubmissionStudent
					(s.getAssignmentid(), s.getStudentid()), "uploadSubmission");
		} 
		else if (m.getCommand().equals("downloadAssignment")) {
			System.out.println("downloading assignment");
			Assignment assgnToDownload = model.get_dbManager().getAssignment((String)m.getContent());
			File f = new File(assgnToDownload.getPath());
			long length = f.length();
			byte[] content = new byte[(int) length];
			assgnToDownload.setDataStream(content);
			return new Message<Assignment>(assgnToDownload, "downloadAssignment");
		} 
		else if(m.getCommand().equals("downloadSubmission"))
		{
			Submission sub = model.get_dbManager().getSubmission((String)m.getContent());
			File f = new File(sub.getPath());
			long length = f.length();
			byte[] content = new byte[(int) length];
			sub.setDataStream(content);
			return new Message<Submission>(sub, "downloadSubmission");
		}
		else if(m.getCommand().equals("addNewAssignment"))
		{
			model.get_dbManager().addAssignment((Assignment) m.getContent());
			return new Message<>(m.getContent(), "addNewAssignment");
		}
		else if(m.getCommand().equals("assignmentActivate"))
		{
			model.get_dbManager().setAssignmentActive(1, (String) m.getContent());
			return new Message<>(null, "assignmentActivate");
		}
		else if(m.getCommand().equals("assignmentDeactivate"))
		{
			model.get_dbManager().setAssignmentActive(0, (String) m.getContent());
			return new Message<>(null, "assignmentDeactivate");
		}
		else if (m.getCommand().equals("professorEmail"))
		{
			model.emailClass((Email) m.getContent());
			return new Message<>(null, "professorEmail");
		}
		else if (m.getCommand().equals("studentEmail"))
		{
			model.emailProfessor((Email) m.getContent());
			return new Message<>(null, "studentEmail");
		}
		else if (m.getCommand().equals("assignmentToDropboxProfessor"))
		{
			System.out.println("professor");
			return new Message<Vector<Submission>>
				(model.get_dbManager().getSubmissionProfessor((String) m.getContent()), "assignmentToDropbox");
		}
		else if(m.getCommand().equals("assignmentToDropboxStudent"))
		{
			return new Message<Vector<Submission>>
				(model.get_dbManager().getSubmissionStudent
						(((InfoHolder) m.getContent()).geta(), ((InfoHolder) m.getContent()).getb()), "assignmentToDropbox");
		}
		else if(m.getCommand().equals("gradeSubmission"))
		{
			Submission s = model.get_dbManager().getSubmission(((InfoHolder)m.getContent()).geta());
			s.setGrade(((InfoHolder) m.getContent()).getb());
			model.get_dbManager().gradeSubmission(s);
			return new Message<>(null, "gradeSubmission");
		}
		else if(m.getCommand().equals("courseToGrades"))
		{
			Vector<Grade> gv = model.get_dbManager().getGrades
					(((InfoHolder)m.getContent()).geta(), ((InfoHolder)m.getContent()).getb());
			return new Message<Vector<Grade>>(gv, "courseToGrades");
		}
		return null;
	}

	/**
	 * A search method using the model. 
	 * @param m The Message to be processed. 
	 * @return The Message containing the result. 
	 */
	private Student lastNameSearch(Message m) {
		return model.get_dbManager().searchStudent((String) m.getContent(), "lastname");
	}

	/**
	 * A search method using the model. 
	 * @param m The Message to be processed. 
	 * @return The Message containing the result. 
	 */
	private Student idSearch(Message m) {
		return model.get_dbManager().searchStudent((String) m.getContent(), "userid");
	}

	/**
	 * A dataBase manipulation method using the model. 
	 * @param m The Message to be processed. 
	 * @return The Message containing the result. 
	 */
	private Vector<Course> createCourse(Message<Course> m) {
		model.get_dbManager().createCourse(m.getContent());
		System.out.println("Course created");
		String result = m.getContent().getProfessor();
		System.out.println("Result is " + result);
		Vector<Course> vectorResult = findCoursesProfessor(result);
		return vectorResult;
	}

	/**
	 * A HomePanel loading method using the model. 
	 * @param m The Message to be processed. 
	 * @return The Message containing the result. 
	 */
	private Message<Vector<Course>> createHomeMessage(LoginInfo l) {
		String c;
		Vector<Course> v;
		if (model.isProfessor(l.getUsername()))
		{
			c = "professorHome";
			v = findCoursesProfessor(l.getUsername());
		}
		else
		{
			c = "studentHome";
			v = findCoursesStudent(l.getUsername()); //this is wrong
		}
			
		return new Message<Vector<Course>>(v, c);
	}

	/**
	 * A search method using the model. 
	 * @param m The Message to be processed. 
	 * @return The Message containing the result. 
	 */
	private Vector<Course> findCoursesProfessor(String id) {
		return model.get_dbManager().getCoursesProfessor(id);
	}
	
	/**
	 * A search method using the model. 
	 * @param m The Message to be processed. 
	 * @return The Message containing the result. 
	 */
	private Vector<Course> findCoursesStudent(String id) {
		return model.get_dbManager().getCoursesStudent(id);
	}

	/**
	 * A display changing method using the view. 
	 * @param l the LoginInfo retrieved from the view
	 */
	public void changeToHome(LoginInfo l) {
		// send login info to client

		view.clear();
		view.displayHome();
	}

}
