package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;

import client.Client;
import shareddataclasses.InfoHolder;
import shareddataclasses.Message;
import view.CoursePanel;
import view.EmailGui;

/**
 * 
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class CoursePanelListener implements ActionListener{

	/**
	 * The CoursePanel this class is listening to
	 */
	private CoursePanel course;
	
	/**
	 * The Client instance using course
	 */
	private Client client;
	
	/**
	 * Creates a CoursePanelListener
	 * @param cour The course
	 * @param c The client
	 */
	public CoursePanelListener(CoursePanel cour, Client c)
	{
		course = cour;
		client = c;
	}
	
	/**
	 * Performs different tasks based on the action from course. 
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == course.getStudentListButton())
		{
			if (client.isStudent())
			{
				client.getView().displayGradesPanel(client.getUserid());
				client.getView().getGradesPanel().addListener
					(new GradesPanelListener(client.getView().getGradesPanel(), client));
				InfoHolder h = new InfoHolder(client.getUserid(), course.getNumber());
				client.analyzeMessage(client.readSockets(new Message<InfoHolder>(h, "courseToGrades")));
			}
			else
			{
				client.getView().displayClassList(course.getNumber());
				client.getView().getSearchResultsPanel().addListener
					(new SearchStudentPanelListener(client.getView().getSearchResultsPanel(), client));
			}
		}
		else if(e.getSource() == course.getEmailButton())
		{
			EmailGui email = new EmailGui(course.getNumber());
			email.addListener(new EmailListener(email, client));
		}
		else if(e.getSource() == course.getAssignmentButton())
		{
			client.getView().displayAssignementList(course.getNumber());
			client.getView().getAssignmentPanel().addListener
				(new AssignmentPanelListener(client.getView().getAssignmentPanel(), client));
			if (client.isStudent())
			{
				client.getView().getAssignmentPanel().changeToStudent();
				client.analyzeMessage(client.readSockets(new Message<String>(course.getNumber(), "courseToAssignmentStudent")));
			}
			else
			{
				client.analyzeMessage(client.readSockets(new Message<String>(course.getNumber(), "courseToAssignmentProfessor")));
			}
		}
		else if(e.getSource() == course.getHomeButton())
		{
			client.getView().displayHome();
		}
		else if(e.getSource() == course.getActivateButton())
		{
			client.analyzeMessage(client.readSockets(new Message<String>(course.getNumber(), "activate")));
			course.showActiveStatus("activated");
		}
		else if(e.getSource() == course.getDeactivateButton())
		{
			client.analyzeMessage(client.readSockets(new Message<String>(course.getNumber(), "deactivate")));
			course.showActiveStatus("deactivated");
		}
		
	}

}
