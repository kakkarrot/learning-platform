package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import client.Client;
import shareddataclasses.Email;
import shareddataclasses.Message;
import view.EmailGui;

/**
 * This is the listener for the panel that sends an email.
 * @author lqf54
 *
 */
public class EmailListener implements ActionListener {

	EmailGui email;
	Client client;
	
	/**
	 * Creates a new EmailListener
	 * @param e
	 * @param c
	 */
	public EmailListener(EmailGui e, Client c)
	{
		email = e;
		client = c;
	}
	
	/**
	 * Does a certain action depending on which part of the panel the user interacts with.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == email.getSend())
			sendEmail();
		else if (e.getSource() == email.getCancel())
			email.dispose();
	}
	
	/**
	 * write the send email function
	 */
	private void sendEmail()
	{
		if (email.getSubject().getText().equals("") || email.getBody().getText().equals(""))
		{
			email.showInvalid();
			return;
		}
		Email e = new Email(email.getSubject().getText(), email.getBody().getText(), email.getCourseid(), client.getUserid());
		String c = "professorEmail";
		if (client.isStudent())
			c = "studentEmail";
		client.analyzeMessage(client.readSockets(new Message<Email>(e, c)));
		email.showSent();
		email.dispose();
	}

}
