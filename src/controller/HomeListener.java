package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import client.Client;
import shareddataclasses.Course;
import shareddataclasses.Message;
import view.AddCoursePanel;
import view.Home;

/**
 * An ActionListener for Home
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class HomeListener implements ActionListener{

	/**
	 * The home to be displayed on the view
	 */
	private Home home;
	
	/**
	 * The Client using home
	 */
	private Client client;
	
	/**
	 * Creates a HomeListener
	 * @param h The Home
	 * @param c The Client
	 */
	public HomeListener(Home h, Client c)
	{
		home = h;
		client = c;
	}
	
	/**
	 * Performs tasks based on the action that occurs within home
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == home.getContinue())
		{
			client.getView().displayCoursePanel((String) home.getCourseList().getSelectedItem());
			client.getView().getCoursePanel().addListener
				(new CoursePanelListener(client.getView().getCoursePanel(), client));
			if (client.isStudent())
				client.getView().getCoursePanel().changeToStudent();
		}
		else if (e.getSource() == home.getCreate())
		{
			homeCreateButton();
		}
	}
	
	/**
	 * Creates an AddCoursePanel when the createButton is used in Home
	 */
	private void homeCreateButton()
	{
		AddCoursePanel panel = createAddCoursePanel();
		if (panel == null)
			return;
		Course c = new Course(panel.getCourse_id().getText(), panel.getCourseName().getText(), client.getUserid(), 0);
		client.analyzeMessage(client.readSockets(new Message<Course>(c, "createCourse")));
	}
	
	/**
	 * Creates an AddCoursePanel when the createButton is used in Home, utility function for homeCreateButton()
	 * @return
	 */
	private AddCoursePanel createAddCoursePanel()
	{
		AddCoursePanel panel = new AddCoursePanel();
		panel.setVisible(true);
		int check = JOptionPane.showConfirmDialog(null, panel, "Add a new course", 
					JOptionPane.OK_CANCEL_OPTION);
		if (check == JOptionPane.CANCEL_OPTION)
			return null;
		else
		{
			if (panel.contentsGood())
				return panel;
			return null;
		}
	}
	
	//below are the getter methods
	
	/**
	 * Gets the Home panel.
	 * @return
	 */
	public Home getHome()
	{
		return home;
	}
	
	/**
	 * Gets the Client
	 * @return
	 */
	public Client getClient()
	{
		return client;
	}

}
