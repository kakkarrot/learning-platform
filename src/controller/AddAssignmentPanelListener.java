package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFileChooser;

import client.Client;
import shareddataclasses.Assignment;
import shareddataclasses.Message;
import view.AddAssignmentPanel;

/**
 * The panel that displays and is used when a professor uploads an assignment.
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class AddAssignmentPanelListener implements ActionListener{

	private AddAssignmentPanel addPanel;
	private Client client;
	private File selectedFile;
	private byte[] temp;
	
	/**
	 * Creates a new AddAssignmentPanelListener
	 * @param a
	 * @param c
	 */
	public AddAssignmentPanelListener(AddAssignmentPanel a, Client c)
	{
		addPanel = a;
		client = c;
		selectedFile = null;
	}
	
	/**
	 * Performs an action based on the message command.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == addPanel.getHomeButton())
		{
			client.getView().clear();
			client.getView().displayHome();
		}
		if (e.getSource() == addPanel.getConfirmButton())
		{
			Assignment a = new Assignment(addPanel.getAssignmentidText().getText(), addPanel.getCourseid(), 
											addPanel.getTitleText().getText(), 
											selectedFile.getAbsolutePath(), 
											addPanel.getDueDateText().getText(), 0);
			if (selectedFile != null && temp != null)
			{
				a.setDataStream(temp);
				client.analyzeMessage(client.readSockets
						(new Message<Assignment>(a, "addNewAssignment")));
			}
		}
		if (e.getSource() == addPanel.getChooseFileButton())
		{
			temp = uploadFile();
		}
		
	}
	
	/**
	 * Opens up JFileChooser, and if successful it uploads the file to the server.
	 */
	private byte[] uploadFile() {
		JFileChooser fileBrowser = new JFileChooser();
		selectedFile = null;
		if (fileBrowser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
			selectedFile = fileBrowser.getSelectedFile();
		}

		if(selectedFile != null)
		{
			long length = selectedFile.length();
			byte[] content = new byte[(int) length];
			// Converting Long to Int
			try {
				FileInputStream fis = new FileInputStream(selectedFile);
				BufferedInputStream bos = new BufferedInputStream(fis);
				bos.read(content, 0, (int) length);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			// 3)Send thearray of bytes overSocketvia an ObjectOutputStream. (Assuming the
			// socket is connected, and the Object Stream is already constructed)
			// Sender(Client?)
			return content;
		}
		
		return null;

	}
	
	//below are the getter methods
	
	public AddAssignmentPanel getAddAssignmentPanel()
	{
		return addPanel;
	}
	
	public Client getClient()
	{
		return client;
	}

}
