package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

import client.Client;
import shareddataclasses.InfoHolder;
import shareddataclasses.Message;
import shareddataclasses.Submission;
import view.DropboxPanel;

public class DropboxPanelListener implements ActionListener {

	private DropboxPanel dropboxPanel;
	private Client client;
	
	public DropboxPanelListener(DropboxPanel d, Client c)
	{
		dropboxPanel = d;
		client = c;
	}
	
	/**
	 * Does a certain action depending on which part of the panel the user interacts with.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if(e.getSource() == dropboxPanel.getBackButton())
		{
			client.getView().displayHome();
		}
		else if (e.getSource() == dropboxPanel.getUploadButton())
		{
			String title = JOptionPane.showInputDialog(null, "Please enter a title for this submission. ");
			Submission s = null;
			if (title == null || title.equals(""))
			{
				return;
			}
			JFileChooser fileBrowser = new JFileChooser();
			File selectedFile = null;
			if (fileBrowser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION) {
				selectedFile = fileBrowser.getSelectedFile();
			}

			if(selectedFile != null)
			{
				long length = selectedFile.length();
				byte[] content = new byte[(int) length];
				// Converting Long to Int
				try {
					FileInputStream fis = new FileInputStream(selectedFile);
					BufferedInputStream bos = new BufferedInputStream(fis);
					bos.read(content, 0, (int) length);
					s = new Submission(dropboxPanel.getAssignmentid(), client.getUserid(), selectedFile.getCanonicalPath(), title);
					s.setDataStream(content);
				} catch (FileNotFoundException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				// 3)Send the array of bytes overSocketvia an ObjectOutputStream. (Assuming the
				// socket is connected, and the Object Stream is already constructed)
				
				Message m = new Message<Submission>(s, "uploadSubmission");
				client.analyzeMessage(client.readSockets(m));
			}
			dropboxPanel.showUploaded();
		}
		else if (e.getSource() == dropboxPanel.getDownloadButton())
		{
			if (dropboxPanel.getList().isSelectionEmpty())
			{
				dropboxPanel.showInvalid();
				return;
			}
			String[] temp = ((String) dropboxPanel.getList().getSelectedValue()).split(" ");
			client.analyzeMessage(client.readSockets
					(new Message<String>(temp[0], "downloadSubmission")));
			client.getView().getDropboxPanel().showDownloaded();
		}
		else if (e.getSource() == dropboxPanel.getGradeButton())
		{
			String grade = JOptionPane.showInputDialog(null, "Please enter a grade out of 100. ");
			if (grade == null)
				return;
			String[] temp = ((String)dropboxPanel.getList().getSelectedValue()).split(" ");
			InfoHolder h = new InfoHolder(temp[0], grade);
			client.analyzeMessage(client.readSockets
					(new Message<InfoHolder>(h, "gradeSubmission")));
			client.getView().getDropboxPanel().showGraded();
		}

	}

}
