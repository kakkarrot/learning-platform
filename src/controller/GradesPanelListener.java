package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import client.Client;
import view.GradesPanel;

/**
 * Listener for the panel that displays submission grades.
 * @author Qifeng Li
 * @author Erik Skoronski
 *
 */
public class GradesPanelListener implements ActionListener {

	private GradesPanel grades;
	private Client client;
	
	/**
	 * Creates a new GradesPanelListener
	 * @param g
	 * @param c
	 */
	public GradesPanelListener(GradesPanel g, Client c)
	{
		grades = g;
		client = c;
	}
	
	/**
	 * Does a certain action depending on which part of the panel the user interacts with.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == grades.getBackButton())
			client.getView().displayHome();
	}

}
