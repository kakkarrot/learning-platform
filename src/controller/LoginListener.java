package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.swing.JOptionPane;

import client.Client;
import model.Model;
import shareddataclasses.LoginInfo;
import shareddataclasses.Message;
import view.LoginGui;

/**
 * An ActionListener for the LoginGui
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class LoginListener implements ActionListener {

	/**
	 * The LoginGui
	 */
	LoginGui login;
	
	/**
	 * The client using the loginGui
	 */
	Client client;

	/**
	 * Creates a LoginListener
	 * @param a The LoginGui
	 * @param c The Client
	 */
	public LoginListener(LoginGui a, Client c) {
		login = a;
		client = c;
	}

	/**
	 * Performs tasks based on actions happening within the loginGui
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == login.getLogin()) {
			Message<?> m = sendLoginMessage();
			if(checkMessage (m))
			{
				client.setUserid(login.get_id().getText());
				client.analyzeMessage(m);
				login.getFrame().dispose();
			}
			else
				login.showInvalid();
		}
	}

	/**
	 * Writes a Message with command login into the socket
	 * @return The Message read from the socket
	 */
	private Message<?> sendLoginMessage() {
		LoginInfo info = new LoginInfo(login.get_id().getText(), new String(login.getPass().getPassword()));
		Message<?> m = new Message<LoginInfo>(info, "login");
		return client.readSockets(m);
	}

	/**
	 * Checks to see if the Message is null or invalid
	 * @param m The Message
	 * @return True if message is neither null or invalid
	 */
	private boolean checkMessage(Message m) {
		if (m == null || m.getCommand().equals("invalidLogin"))
			return false;
		return true;
	}

}
