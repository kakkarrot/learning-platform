package model;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;
import java.util.Vector;

import shareddataclasses.Assignment;
import shareddataclasses.Course;
import shareddataclasses.Grade;
import shareddataclasses.LoginInfo;
import shareddataclasses.Professor;
import shareddataclasses.Student;
import shareddataclasses.Submission;

/**
 * Connects to database and does database stuff.
 * This is where the magic happens.
 * 
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class DataBaseManager {
	private java.sql.Connection connection;
	private java.sql.PreparedStatement preparedStatement;

	public String connectionInfo = "jdbc:mysql://localhost:3306/learningplatformdb", login = "root",
			password = "ENSF409#Lab8";

	/**
	 * Connects to mySQL server safely
	 */
	public DataBaseManager() {
		try {
			// If this throws an error, make sure you have added the mySQL connector JAR to
			// the project
			Class.forName("com.mysql.jdbc.Driver");

			// If this fails make sure your connectionInfo and login/password are correct
			connection = DriverManager.getConnection(connectionInfo, login, password);
			System.out.println("Connected to: " + connectionInfo + "\n");
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * searches for a professor in the table
	 * @param id
	 * @return
	 */
	public Professor getProfessor(String id) {
		String sql = "SELECT * FROM USERTABLE WHERE USERID=?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				return new Professor(rs.getString("USERID"), rs.getString("EMAIL"), rs.getString("FIRSTNAME"),
						rs.getString("LASTNAME"));
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Returns the professor ID for a course.
	 * @param id
	 * 		The course's ID.
	 * @return
	 * 		Professors ID.
	 */
	public String getCourseProfessorid(String id)
	{
		String sql = "SELECT PROFESSORID FROM COURSETABLE WHERE COURSEID=?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				return rs.getString("PROFESSORID");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Returns an string of all emails in the class, formatted and separated by commas.
	 * @param id 
	 * 		Is the course ID.
	 * @return 
	 * 		String of all emails in the class separated by commas,
	 */
	public String getClassEmail(String id)
	{
		String sql = "SELECT EMAIL FROM USERTABLE AS UT INNER JOIN STUDENTENROLLMENTTABLE AS SE "
				+ "ON UT.USERID = SE.STUDENTID WHERE COURSEID=?";
		String results = "";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			ResultSet rs = preparedStatement.executeQuery();
			for (;rs.next();) {
				results += rs.getString("EMAIL"); 
				if (!rs.isLast())
					results += ", ";
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return results;
	}
	
	/**
	 * Returns an email from the user table, searches by userID.
	 * @param id 
	 * 		A user's ID
	 * @return 
	 * 		Returns an email in the form of a string,
	 */
	public String getEmail(String id)
	{
		String sql = "SELECT EMAIL FROM USERTABLE WHERE USERID=?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				return rs.getString("EMAIL");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Returns the password for a user (yes we know this is terrible security)
	 * @param id
	 * 		The user ID.
	 * @return 
	 * 		The user's password.
	 */
	public String getPassword(String id)
	{
		String sql = "SELECT PASSWORD FROM USERTABLE WHERE USERID=?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				return rs.getString("PASSWORD");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * gets the type associated with the id
	 * 
	 * @param id
	 *      the id
	 * @return 
	 * 		the type of user the id is associated with
	 */
	protected String getType(String id) {
		String sql = "SELECT TYPE FROM USERTABLE WHERE USERID=?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				return rs.getString("TYPE");
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return null;
	}

	/**
	 * Validates whether or not login info is valid.
	 * @param l 
	 * 		Login info - contains a user name and a password.
	 * @return 
	 * 		True or false, depending on whether or not the login info is valid.
	 */
	protected boolean validate(LoginInfo l) // works
	{
		String sql = "SELECT PASSWORD FROM USERTABLE WHERE USERID=?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, l.getUsername());
			ResultSet rs = preparedStatement.executeQuery();
			if (rs.next()) {
				if (rs.getString("PASSWORD").equals(l.getPassword()))
					return true;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		return false;
	}

	/**
	 * Searches for a student in the user table.
	 * @param key 
	 * 		Either the user ID or a student's last name.
	 * @param choice 
	 * 		Specifies what the method searches by.
	 * @return
	 */
	public Student searchStudent(String key, String choice) {
		String sql = "SELECT * FROM USERTABLE WHERE " + choice + " LIKE ? AND TYPE LIKE \'S\'";
		ResultSet rs;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, key);
			rs = preparedStatement.executeQuery();
			if ( rs.next()) {
				{
					return new Student(rs.getString("USERID"), rs.getString("EMAIL"), 
									   rs.getString("FIRSTNAME"), rs.getString("LASTNAME"));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Adds a course into the database
	 * @param c 
	 * 		The course
	 */
	public void createCourse(Course c) {
		String sql = "INSERT INTO COURSETABLE (COURSEID, PROFESSORID, COURSENAME, ACTIVE) " + "VALUES (?, ?, ?, ?) ";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, c.getNumber());
			preparedStatement.setString(2, c.getProfessor());
			preparedStatement.setString(3, c.getName());
			preparedStatement.setInt(4, c.getActive());
			preparedStatement.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * changes the active parameter of a course
	 * @param active 
	 * 		The active bit that the course should be set to.
	 * @param id 
	 * 		The course ID
	 */
	public void setCourseActive(int active, String id) {
		String sql = "UPDATE COURSETABLE SET ACTIVE=? WHERE COURSEID=?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, active);
			preparedStatement.setString(2, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Changes the active parameter of an assignment
	 * @param active 
	 * 		The active bit that the course should be set to.
	 * @param id 
	 * 		The assignment ID
	 */
	public void setAssignmentActive(int active, String id) {
		String sql = "UPDATE ASSIGNMENTTABLE SET ACTIVE=? WHERE ASSIGNMENTID=?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setInt(1, active);
			preparedStatement.setString(2, id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Adds a student course combination to the enrollment table
	 * @param s_id
	 * 		This is the student ID
	 * @param c_id
	 * 		This is the course ID
	 */
	public void addToEnrollment(String s_id, String c_id)
	{
		String sql = "INSERT INTO STUDENTENROLLMENTTABLE "
				+ " (ENROLLMENTID, STUDENTID, COURSEID) "
				+ "VALUES (?, ?, ?) ";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, s_id+c_id);
			preparedStatement.setString(2, s_id);
			preparedStatement.setString(3, c_id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Adds a student course combination to the enrollment table
	 * @param s_id
	 * 		This is the student ID
	 * @param c_id
	 * 		This is the course ID
	 */
	public boolean isEnrolled(String s_id, String c_id)
	{
		String sql = "SELECT * FROM STUDENTENROLLMENTTABLE WHERE ENROLLMENTID=?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, s_id+c_id);
			ResultSet rs = preparedStatement.executeQuery();
			if(rs.next())
			{
				return true;
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Adds a user to the database. This method is only used to add users for testing, not called in operation.
	 * @param id
	 * 		The user's ID.
	 * @param pw
	 * 		The user's password.
	 * @param em
	 * 		The user's email. 
	 * @param fn
	 * 		The user's first name.
	 * @param ln
	 * 		The user's last name.
	 * @param t
	 * 		The user's type (either p for professor or s for student).
	 */
	public void addUser(String id, String pw, String em, String fn, String ln, String t)
	{
		String sql = "INSERT INTO USERTABLE "
				+ " (USERID, PASSWORD, EMAIL, FIRSTNAME, LASTNAME, TYPE) "
				+ "VALUES (?, ?, ?, ?, ?, ?) ";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			preparedStatement.setString(2, pw);
			preparedStatement.setString(3, em);
			preparedStatement.setString(4, fn);
			preparedStatement.setString(5, ln);
			preparedStatement.setString(6, t);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Removes the student course combination from the enrollment table
	 * @param s_id
	 * 		The student's ID.
	 * @param c_id
	 * 		The course's ID.
	 */
	public void removeFromEnrollment(String s_id, String c_id)
	{
		String sql = "DELETE FROM STUDENTENROLLMENTTABLE WHERE ENROLLMENTID=?";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, s_id+c_id);
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Returns all of the courses that a student is enrolled in.
	 * @param id
	 * 		The course's ID.
	 * @return
	 * 		Vector of courses the student is enrolled in.
	 */
	public Vector<Course> getCoursesStudent(String id)
	{
		String sql = "SELECT * FROM STUDENTENROLLMENTTABLE INNER JOIN COURSETABLE "
					+ "ON STUDENTENROLLMENTTABLE.COURSEID = COURSETABLE.COURSEID "
					+ "WHERE STUDENTENROLLMENTTABLE.STUDENTID=? AND COURSETABLE.ACTIVE=1";
		Vector<Course> v = new Vector<Course>();
		ResultSet rs;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			rs = preparedStatement.executeQuery();
			for (; rs.next();) {
				{
					System.out.println(rs.getString("COURSENAME"));
					v.add(new Course(rs.getString("COURSEID"), rs.getString("COURSENAME"), id, rs.getInt("ACTIVE")));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return v;
	}
	
	/**
	 * Gets a vector of all of the courses a professor teaches.
	 * @param id
	 * 		The course ID.
	 * @return
	 * 		A vector of a courses the professor teachers.
	 */
	public Vector<Course> getCoursesProfessor(String id)
	{
		String sql = "SELECT * FROM COURSETABLE WHERE PROFESSORID=?";
		Vector<Course> v = new Vector<Course>();
		ResultSet rs;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			rs = preparedStatement.executeQuery();
			for (; rs.next();) {
				{
					System.out.println(rs.getString("COURSENAME"));
					v.add(new Course(rs.getString("COURSEID"), rs.getString("COURSENAME"), id, rs.getInt("ACTIVE")));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return v;
	}
	
	/**
	 * Adds assignment to database.
	 * @param a Assignemt to be added to database.
	 */
	public void addAssignment(Assignment a)
	{
		String sql = "INSERT INTO ASSIGNMENTTABLE "
				+ " (ASSIGNMENTID, COURSEID, TITLE, PATH, ACTIVE, DUEDATE) "
				+ "VALUES (?, ?, ?, ?, ?, ?) ";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, a.getAssignment_id());
			preparedStatement.setString(2, a.getCourse_id());
			preparedStatement.setString(3, a.getTitle());
			preparedStatement.setString(4, a.getPath());
			preparedStatement.setInt(5, a.getActive());
			preparedStatement.setString(6, a.getDueDate());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets assignment from database.
	 * @param id
	 * 		The assignment ID.
	 * @return
	 * 		The found assignment from the database.
	 */
	public Assignment getAssignment(String id)
	{
		String sql = "SELECT * FROM ASSIGNMENTTABLE WHERE ASSIGNMENTID=?";
		ResultSet rs;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			rs = preparedStatement.executeQuery();
			if (rs.next()) {
				{
					return new Assignment(rs.getString("ASSIGNMENTID"), rs.getString("COURSEID"), 
							rs.getString("TITLE"), rs.getString("PATH"), rs.getString("DUEDATE"), rs.getInt("ACTIVE"));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Gets all assignments that a professor has uploaded.
	 * @param id
	 * 		The professor's ID.
	 * @return
	 * 		The assignments that the professor uploaded.
	 */
	public Vector<Assignment> getAssignmentProfessor(String id)
	{
		String sql = "SELECT * FROM ASSIGNMENTTABLE WHERE COURSEID=?";
		String results = "";
		Vector<Assignment> v = new Vector<Assignment>();
		ResultSet rs;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			rs = preparedStatement.executeQuery();
			for (; rs.next();) {
				{
					v.add(new Assignment(rs.getString("ASSIGNMENTID"), rs.getString("COURSEID"), 
							rs.getString("TITLE"), rs.getString("PATH"), rs.getString("DUEDATE"), rs.getInt("ACTIVE")));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return v;
	}
	
	/**
	 * Gets all assignments that a student has to do.
	 * @param id
	 * 		The student's ID.
	 * @return
	 * 		The assignments that the student completed.
	 */
	public Vector<Assignment> getAssignmentStudent(String id)
	{
		String sql = "SELECT * FROM ASSIGNMENTTABLE WHERE COURSEID=? AND ACTIVE=1";
		String results = "";
		Vector<Assignment> v = new Vector<Assignment>();
		ResultSet rs;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			rs = preparedStatement.executeQuery();
			for (; rs.next();) {
				{
					v.add(new Assignment(rs.getString("ASSIGNMENTID"), rs.getString("COURSEID"), 
							rs.getString("TITLE"), rs.getString("PATH"), rs.getString("DUEDATE"), rs.getInt("ACTIVE")));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return v;
	}
	
	/**
	 * Gets all of the submissions students have uploaded for an assignment. Used for grading.
	 * @param id
	 * 		A courses's ID.
	 * @return
	 * 		Returns a vector of the submissions.
	 */
	public Vector<Submission> getSubmissionProfessor(String id)
	{
		String sql = "SELECT * FROM SUBMISSIONTABLE WHERE ASSIGNMENTID=?";
		Vector<Submission> v = new Vector<Submission>();
		ResultSet rs;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			rs = preparedStatement.executeQuery();
			for (; rs.next();) {
				{
					v.add(new Submission(rs.getString("SUBMISSIONID"), rs.getString("ASSIGNMENTID"), 
										 rs.getString("STUDENTID"), rs.getString("PATH"), 
										 rs.getString("TITLE"), rs.getString("TIMESTAMP"), rs.getString("GRADE")));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return v;
	}
	
	/**
	 * Gets all of the submissions a student has uploaded for an assignment.
	 * @param aid
	 * 		A assignment's ID.
	 * @param sid
	 * 		A student's ID.
	 * @return
	 * 		Returns a vector of their submissions.
	 */
	public Vector<Submission> getSubmissionStudent(String aid, String sid)
	{
		String sql = "SELECT * FROM SUBMISSIONTABLE WHERE ASSIGNMENTID=? AND STUDENTID=?";
		Vector<Submission> v = new Vector<Submission>();
		ResultSet rs;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, aid);
			preparedStatement.setString(2, sid);
			rs = preparedStatement.executeQuery();
			for (; rs.next();) {
				{
					v.add(new Submission(rs.getString("SUBMISSIONID"), rs.getString("ASSIGNMENTID"), 
							 rs.getString("STUDENTID"), rs.getString("PATH"), 
							 rs.getString("TITLE"), rs.getString("TIMESTAMP"), rs.getString("GRADE")));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return v;
	}
	
	/**
	 * Gets a submission from the database.
	 * @param id
	 * 		Submission ID.
	 * @return
	 * 		The found submission.
	 */
	public Submission getSubmission(String id)
	{
		String sql = "SELECT * FROM SUBMISSIONTABLE WHERE SUBMISSIONID=?";
		ResultSet rs;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, id);
			rs = preparedStatement.executeQuery();
			if (rs.next()) {
				{
					return new Submission(rs.getString("SUBMISSIONID"), rs.getString("ASSIGNMENTID"), 
							 rs.getString("STUDENTID"), rs.getString("PATH"), 
							 rs.getString("TITLE"), rs.getString("TIMESTAMP"), rs.getString("GRADE"));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * Adds a submission to the database.
	 * @param s
	 * 		The submission.
	 */
	public void addSubmission(Submission s)
	{
		String sql = "INSERT INTO SUBMISSIONTABLE "
				+ " (SUBMISSIONID, ASSIGNMENTID, STUDENTID, PATH, TITLE, GRADE, TIMESTAMP) "
				+ "VALUES (?, ?, ?, ?, ?, ?, ?) ";
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, s.getSubmissionid());
			preparedStatement.setString(2, s.getAssignmentid());
			preparedStatement.setString(3, s.getStudentid());
			preparedStatement.setString(4, s.getPath());
			preparedStatement.setString(5, s.getTitle());
			preparedStatement.setString(6, s.getGrade());
			preparedStatement.setString(7, s.getTimeStamp());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/** Grades a submission.
	 * 
	 * @param s
	 * 		The submission,
	 */
	public void gradeSubmission(Submission s)
	{
		String sql = "INSERT INTO GRADESTABLE "
				+ " (GRADEID, ASSIGNMENTID, STUDENTID, COURSEID, ASSIGNMENTGRADE) "
				+ "VALUES (?, ?, ?, ?, ?) ";
		Assignment a = getAssignment(s.getAssignmentid());
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, s.getSubmissionid() + s.getGrade());
			preparedStatement.setString(2, s.getAssignmentid());
			preparedStatement.setString(3, s.getStudentid());
			preparedStatement.setString(4, a.getCourse_id());
			preparedStatement.setString(5, s.getGrade());
			preparedStatement.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Gets all of the grades for a student.
	 * @param sid
	 * 		Student ID.
	 * @param cid
	 * 		Course ID.
	 * @return
	 * 		Returns a vector of grades for the student and course.
	 */
	public Vector<Grade> getGrades(String sid, String cid)
	{
		String sql = "SELECT * FROM GRADESTABLE WHERE STUDENTID=? AND COURSEID=?";
		Vector<Grade> v = new Vector<Grade>();
		ResultSet rs;
		try {
			preparedStatement = connection.prepareStatement(sql);
			preparedStatement.setString(1, sid);
			preparedStatement.setString(2, cid);
			rs = preparedStatement.executeQuery();
			for (; rs.next();) {
				{
					Assignment a = this.getAssignment(rs.getString("ASSIGNMENTID"));
					v.add(new Grade(a.getTitle(), rs.getString("COURSEID"), 
							 rs.getString("STUDENTID"), rs.getString("ASSIGNMENTGRADE")));
				}
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}
		return v;
	}

	
//	public static void main(String[] args)
//	{
//		DataBaseManager d = new DataBaseManager();
//		System.out.println(d.getClassEmail("233"));
//	}
	
}
