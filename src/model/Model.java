package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import shareddataclasses.Course;
import shareddataclasses.LoginInfo;
import shareddataclasses.Message;
import shareddataclasses.Professor;
import shareddataclasses.Student;
import shareddataclasses.Email;

/**
 * The base model for the LearningPlatformMVC
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Model {

	/**
	 * The DataBaseManager
	 */
	protected DataBaseManager dbManager;
	
	/**
	 * The EmailHelper
	 */
	protected EmailHelper emailer;
	
	/**
	 * Creates a Model
	 */
	public Model()
	{
		dbManager = new DataBaseManager();
	}
	
	
	/**
	 * Gets the professor associated with an id
	 * @param id The id
	 * @return The professor
	 */
	public Professor getProfessor(String id)
	{
		if(!isProfessor(id))
			return null;
		return dbManager.getProfessor(id);
	}
	
	/**
	 * checks to see if user is student
	 * @param id the id
	 * @return true if id corresponds to a student
	 */
	public boolean isStudent(String id)
	{
		if (dbManager.getType(id) != null && dbManager.getType(id).equals("s"))
			return true;
		return false;
	}
	
	/**
	 * checks to see if user is professor
	 * @param id the id
	 * @return true if id corresponds to professor
	 */
	public boolean isProfessor(String id)
	{
		if (dbManager.getType(id) != null && dbManager.getType(id).equals("p"))
			return true;
		return false;
	}
	
	/**
	 * checks if login info is correct
	 * @param l the login info
	 * @return true if user is valid
	 */
	public boolean checkLoginInfo(LoginInfo l)
	{
		return dbManager.validate(l);
	}
	
	/**
	 * Sends an email to the class
	 * @param email The user email
	 */
	public void emailClass(Email email)
	{
		String e = dbManager.getEmail(email.getUserid());
		String p = dbManager.getPassword(email.getUserid());
		emailer = new EmailHelper(e, p);
		String recipients = dbManager.getClassEmail(email.getCourseid());
		emailer.sendEmail(e, recipients, email);
	}
	
	/**
	 * Sends an email to the course professor
	 * @param email The user email
	 */
	public void emailProfessor(Email email)
	{
		String e = dbManager.getEmail(email.getUserid());
		String p = dbManager.getPassword(email.getUserid());
		emailer = new EmailHelper(e, p);
		String prof = dbManager.getCourseProfessorid(email.getCourseid());
		String recipients = dbManager.getEmail(prof);
		emailer.sendEmail(e, recipients, email);
	}
	
	//below are getter methods
	
	public DataBaseManager get_dbManager()
	{
		return dbManager;
	}
}
