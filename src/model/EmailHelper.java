package model;

import java.util.Properties;

import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import shareddataclasses.Email;
import shareddataclasses.Message;
/**
 * Helps with tasks involving sending emails
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class EmailHelper {

	/**
	 * The instance of the person sending the emails.
	 */
	private	Session session;
	
	/**
	 * The criteria needed to connect to an email service. 
	 */
	private Properties properties;
	
	/**
	 * Creates an EmailHelper
	 * @param e The sender email
	 * @param p The sender password
	 */
	public EmailHelper(String e, String p)
	{
		setupProperties();
		setupSession(e, p);
	}
	
	/**
	 * Sets up properties
	 */
	private void setupProperties()
	{
		properties = new Properties();
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.auth", "true");
//		properties.put("mail.smtp.socketFactory.port", "465");
//		properties.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		properties.put("mail.smtp.host", "smtp.gmail.com");
//		properties.put("mail.smtp.port", "465");
		properties.put("mail.smtp.port", "587");
	}
	
	/**
	 * Sets up session
	 * @param email The email of the sender
	 * @param password The password of the sender
	 */
	private void setupSession(String email, String password)
	{
		session = Session.getInstance(properties, 
				new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(email, password);
			}
		});
	}
	
	/**
	 * Sends an email to the recipients
	 * @param email The sender email
	 * @param recipients The list of recievers
	 * @param m The email
	 */
	public void sendEmail(String email, String recipients, Email m)
	{
		try {
			javax.mail.Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(email));
			message.addRecipients(javax.mail.Message.RecipientType.TO, InternetAddress.parse(recipients));
			message.setSubject(m.getSubject());
			message.setText(m.getContent());
			Transport.send(message);
		} catch (MessagingException e)
		{
			e.printStackTrace();
		}
	}
}
