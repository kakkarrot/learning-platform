package model;

import java.sql.ResultSet;
import java.sql.SQLException;

import shareddataclasses.Course;
import shareddataclasses.Professor;
import shareddataclasses.Student;

/**
 * Contain logic for the professorGui
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class ProfessorModel extends Model {

	/**
	 * A search function for the professor. Searches by either ID or lastname based
	 * on choice.
	 * 
	 * @param key
	 *            the search criteria
	 * @param choice
	 *            the search type
	 * @return
	 * 			  the student with matching criteria
	 */			  
	public Student searchStudent(String key, String choice) {
		return dbManager.searchStudent(key, choice);
	}

	/**
	 * Creates a course in the database
	 * @param c the course
	 */
	public void createCourse(Course c) {
		dbManager.createCourse(c);
	}

	/**
	 * Sets a course as active in the database
	 * @param id The course id
	 */
	public void setActive(String id) {
//		dbManager.setActive(id);
	}

}
