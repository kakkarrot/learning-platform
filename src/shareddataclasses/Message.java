package shareddataclasses;

import java.io.Serializable;
/**
 * A generic data holder class
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Message<T> implements Serializable{

	/**
	 * A generated id
	 */
	private static final long serialVersionUID = 8092489146456685407L;
	
	/**
	 * the generic content
	 */
	private T content;
	
	/**
	 * the command
	 */
	private String command;
	
	/**
	 * Creates an object of type message
	 * @param t content
	 * @param s command
	 */
	public Message(T t, String s)
	{
		content = t;
		command = s;
	}
	
	//below are the getter methods
	
	public T getContent()
	{
		return content;
	}
	
	public String getCommand()
	{
		return command;
	}

}
