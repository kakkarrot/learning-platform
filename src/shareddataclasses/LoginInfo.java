package shareddataclasses;

import java.io.Serializable;
/**
 * A data holder class
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class LoginInfo implements Serializable {
	
	/**
	 * A generated id
	 */
	private static final long serialVersionUID = 4234122678649349068L;
	
	/**
	 * username
	 */
	private String username;
	
	/**
	 * password
	 */
	private String password;
	
	/**
	 * constructor for login info
	 * @param u the username
	 * @param p the password
	 */
	public LoginInfo(String u, String p)
	{
		username = u;
		password = p;
	}
	
	//below are some getter functions
	public String getUsername()
	{
		return username;
	}
	
	public String getPassword()
	{
		return password;
	}
	
}
