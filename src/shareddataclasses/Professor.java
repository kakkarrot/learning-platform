package shareddataclasses;

import java.io.Serializable;
/**
 * A data holder class
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Professor extends User implements Serializable {
	
	/**
	 * Creates an object of type professor
	 * @param i id
	 * @param e email
	 * @param fn firstName
	 * @param ln lastName
	 */
	public Professor(String i, String e, String fn, String ln)
	{
		id = i;
		email = e;
		firstName = fn;
		lastName = ln;
		type = "p";
	}
	
	public String toString()
	{
		return super.toString() + " " + "Professor";
	}
	
}
