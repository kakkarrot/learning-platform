package shareddataclasses;

import java.io.Serializable;
/**
 * A data holder object
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Assignment implements Serializable {
		
	/**
	 * A random generated id
	 */
	private static final long serialVersionUID = 1672578405306707578L;
	
	/**
	 * assignment id
	 */
	private String assignment_id;
	
	/**
	 * course id
	 */
	private String course_id;
	
	/**
	 * title
	 */
	private String title;
	
	/**
	 * path
	 */
	private String path;
	
	/**
	 * due date
	 */
	private String dueDate;
	
	/**
	 * active status
	 */
	private int active;
	
	/**
	 * File converted to byteStream
	 */
	private byte[] dataStream;
	
	/**
	 * Creates an object of type Assignment
	 * @param i assignment_id
	 * @param c course_id
	 * @param t title
	 * @param p path
	 * @param d dueDate
	 * @param a active
	 */
	public Assignment(String i, String c, String t, String p, String d, int a)
	{
		assignment_id = i;
		course_id = c;
		title = t;
		path = p;
		dueDate = d;
		active = a;
	}
	
	//below are the getter methods
	public String getAssignment_id()
	{
		return assignment_id;
	}
	
	public String getCourse_id()
	{
		return course_id;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public String getPath()
	{
		return path;
	}
	
	public String getDueDate()
	{
		return dueDate;
	}
	
	public int getActive()
	{
		return active;
	}
	
	public byte[] getDataStream() {
		return dataStream;
	}

	public void setDataStream(byte[] content) {
		dataStream = content;
	}
	
}
