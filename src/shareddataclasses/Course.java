package shareddataclasses;

import java.io.Serializable;
/**
 * A data holder object
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Course implements Serializable {
	/**
	 * A generated id
	 */
	private static final long serialVersionUID = -5178828318143832430L;
	
	/**
	 * number
	 */
	private String number;
	
	/**
	 * name
	 */
	private String name;
	
	/**
	 * professor name
	 */
	private String professorName;
	
	/**
	 * active status
	 */
	private int active;
	
	/**
	 * Creates an object of type course
	 * @param id number
	 * @param n name
	 * @param prof professorName
	 * @param a active
	 */
	public Course(String id, String n, String prof, int a)
	{
		number = id;
		name = n;
		professorName = prof;
		active = a;
	}
	
	//below are the getter methods
	
	public String getNumber()
	{
		return number;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getProfessor()
	{
		return professorName;
	}
	
	public int getActive()
	{
		return active;
	}
}
