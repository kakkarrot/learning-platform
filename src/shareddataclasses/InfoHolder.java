package shareddataclasses;

import java.io.Serializable;
/**
 * An data holder object
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class InfoHolder implements Serializable{
	/**
	 * A generated id
	 */
	private static final long serialVersionUID = -1137390208737420912L;
	
	/**
	 * a
	 */
	private String a;
	
	/**
	 * b
	 */
	private String b;
	
	/**
	 * c
	 */
	private String c;
	
	/**
	 * Creates an object of type InfoHolder
	 * @param o a
	 * @param t b
	 */
	public InfoHolder(String o, String t)
	{
		a = o;
		b = t;
	}
	
	//below are the getter/setter methods
	
	public void setc(String s)
	{
		c = s;
	}
	
	public String getc()
	{
		return c;
	}
	
	public String geta()
	{
		return a;
	}
	
	public String getb()
	{
		return b;
	}
}
