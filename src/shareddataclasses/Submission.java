package shareddataclasses;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
/**
 * A data holder object
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Submission  implements Serializable {
	/**
	 * submission id
	 */
	private String submissionid;
	
	/**
	 * assignment id
	 */
	private String assignmentid;
	
	/**
	 * student id
	 */
	private String studentid;
	
	/**
	 * path
	 */
	private String path;
	
	/**
	 * title
	 */
	private String title;
	
	/**
	 * timeStamp
	 */
	private String timeStamp;
	
	/**
	 * grade
	 */
	private String grade;
	
	/**
	 * File to byte stream
	 */
	private byte[] dataStream;
	
	/**
	 * Creates an object of type submission
	 * @param aid assignmentid
	 * @param sid studentid
	 * @param p path
	 * @param tit title
	 */
	public Submission(String aid, String sid, String p, String tit)
	{
		submissionid = aid + sid;
		assignmentid = aid;
		studentid = sid;
		path = p;
		title = tit;
		Date date = new Date();
		DateFormat df = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		timeStamp = df.format(date);
	}
	
	/**
	 * Creates an object of type submission
	 * @param subid submissionid
	 * @param aid assignmentid
	 * @param sid studentid
	 * @param p path
	 * @param tit title
	 * @param d timeStamp
	 * @param g grade
	 */
	public Submission(String subid, String aid, String sid, String p, String tit, String d, String g)
	{
		submissionid = subid;
		assignmentid = aid;
		studentid = sid;
		path = p;
		title = tit;
		timeStamp = d;
		grade = g;
	}
	
	//below are the getter/setter methods
	
	public String getSubmissionid()
	{
		return submissionid;
	}
	
	public String getAssignmentid()
	{
		return assignmentid;
	}
	
	public String getStudentid()
	{
		return studentid;
	}
	
	public String getPath()
	{
		return path;
	}
	
	public String getTitle()
	{
		return title;
	}
	
	public String getTimeStamp()
	{
		return timeStamp;
	}
	
	public String getGrade()
	{
		return grade;
	}
	
	public void setGrade(String g)
	{
		grade = g;
	}
	
	public void setDataStream(byte[] b)
	{
		dataStream = b;
	}
	
	public byte[] getDataStream() {
		return dataStream;
	}

}
