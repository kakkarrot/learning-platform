package shareddataclasses;

import java.io.Serializable;
/**
 * A data holder class
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public abstract class User implements Serializable {
	/**
	 * id
	 */
	protected String id;
	
	/**
	 * email
	 */
	protected String email;
	
	/**
	 * firstName, lastName
	 */
	protected String firstName, lastName;
	
	/**
	 * type
	 */
	protected String type;
	
	public String toString()
	{
		return id + " " + email + " " + firstName + " " + lastName;
	}
	
	//below are the getter methods
	
	public String get_id()
	{
		return id;
	}
	
	public String getEmail()
	{
		return email;
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public String getType()
	{
		return type;
	}
	
}
