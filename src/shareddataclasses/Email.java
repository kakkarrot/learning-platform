package shareddataclasses;

import java.io.Serializable;
/**
 * A data holder object
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Email  implements Serializable {
	/**
	 * subject
	 */
	private String subject;
	
	/**
	 * content
	 */
	private String content;
	
	/**
	 * course id
	 */
	private String courseid;
	
	/**
	 * user id
	 */
	private String userid;
	
	/**
	 * Creates an object of type email
	 * @param s subject
	 * @param c content
	 * @param id courseid
	 * @param u userid
	 */
	public Email(String s, String c, String id, String u)
	{
		subject = s;
		content  = c;
		courseid = id;
		userid = u;
	}
	
	//below are the getter/setter methods

	public String getSubject()
	{
		return subject;
	}
	
	public String getContent()
	{
		return content;
	}
	
	public String getCourseid()
	{
		return courseid;
	}
	
	public String getUserid()
	{
		return userid;
	}
	
	public void setSubject(String s)
	{
		subject = s;
	}
	
	public void setContent(String s)
	{
		content = s;
	}
	
}
