package shareddataclasses;

import java.io.Serializable;
/**
 * A data holder object
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Grade implements Serializable {
	/**
	 * A generated id
	 */
	private static final long serialVersionUID = 3268673633666684972L;
	
	/**
	 * assignment id
	 */
	private String assignment_id;
	
	/**
	 * student id
	 */
	private String student_id;
	
	/**
	 * course id
	 */
	private String course_id;
	
	/**
	 * grade
	 */
	private String grade;

	/**
	 * Creates an object of type grade
	 * @param a assignment_id
	 * @param s student_id
	 * @param c course_id
	 * @param g grade
	 */
	public Grade(String a, String s, String c, String g)
	{
		assignment_id = a;
		student_id = s;
		course_id = c;
		grade = g;
	}
	
	//below are the getter methods
	public String getAssignment_id()
	{
		return assignment_id;
	}

	public String getStudent_id() {
		return student_id;
	}

	public String getCourse_id() {
		return course_id;
	}
	
	public String getGrade()
	{
		return grade;
	}
	
}
