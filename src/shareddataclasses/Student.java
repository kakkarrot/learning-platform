package shareddataclasses;

import java.io.Serializable;
/**
 * A data holder class
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Student extends User implements Serializable {

	/**
	 * Creates an object of type student
	 * @param i id
	 * @param e email
	 * @param fn firstName
	 * @param ln lastName
	 */
	public Student(String i, String e, String fn, String ln)
	{
		id = i;
		email = e;
		firstName = fn;
		lastName = ln;
		type = "s";
	}
	
	public String toString()
	{
		return super.toString() + " " + "Student";
	}
	
}
