package server;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import controller.Controller;

/**
 * Class server opens and accepts a socket connection through port 9090 and is
 * responsible for returning a condition to client.
 * 
 *
 * @author Erik Skoronski
 * @author Qifeng Li
 */

public class Server {

	/**
	 * The socket to write to
	 */
	private Socket socket;
	
	/**
	 * The serverSocket that accepts the socket
	 */
	private ServerSocket serverSocket;
	
	/**
	 * The ObjectInputStream
	 */
	private ObjectInputStream socketIn;
	
	/**
	 * The ObjectOutputStream
	 */
	private ObjectOutputStream socketOut;
	
	/**
	 * The threadpool
	 */
	ExecutorService pool = Executors.newCachedThreadPool();

	/**
	 * Creates a server
	 */
	public Server() { // throws IOException {
		try {
			serverSocket = new ServerSocket(9091);
		} catch (IOException e) {
			e.getMessage();
			System.out.println("Create the new socket error");
		}
		System.out.println("Server is running...");
	}

	/**
	 * This function is called by main to run the server. It ends when the threads
	 * end.
	 */
	public void runServer() {
		while (true) {
			try {
				socket = serverSocket.accept();
				socketOut = new ObjectOutputStream(socket.getOutputStream());
				socketIn = new ObjectInputStream(socket.getInputStream());

				 Controller session = new Controller(socketIn, socketOut);
				 pool.execute(session);
				 
			} catch (Exception e) {
				e.printStackTrace();
				break;
			}
		}

	}

	/**
	 * Main function
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Server learningPlatformServer = new Server();
		learningPlatformServer.runServer();
	}

}
