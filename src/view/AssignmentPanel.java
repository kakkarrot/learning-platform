package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import client.Client;
import controller.AssignmentPanelListener;

import java.awt.SystemColor;

/**
 * A custom JPanel for displaying assignments
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class AssignmentPanel extends JPanel {
	private DefaultListModel<String> model;
	private JScrollPane assignments;
	private JTextArea searchResults;
	private JList<String> listArea;
	private JList list;
	private JButton deactiveButton;
	private JButton backButton;
	private JButton uploadButton;
	private JButton downloadButton;
	private JButton dropboxButton;
	private JButton activeButton;
	private JTextField courseName;
	private Client client;
	private String courseid;

	/**
	 * Create the panel.
	 */
	public AssignmentPanel(String s, Client c) {
		courseid = s;
		client = c;
		setPreferredSize(new Dimension(724, 600));
		setLayout(null);

		model = new DefaultListModel<String>();
		listArea = new JList<String>(model);
		listArea.setPrototypeCellValue("123456789012345678901234567890");
		listArea.setVisibleRowCount(10);
		searchResults = new JTextArea(100, 100);
		assignments = new JScrollPane(listArea);

		list = new JList(model);
		list.setBounds(29, 76, 541, 225);
		add(list);

		deactiveButton = new JButton("Deactivate");
		deactiveButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		deactiveButton.setBounds(473, 346, 97, 25);
		add(deactiveButton);

		backButton = new JButton("Home");
		backButton.setBounds(42, 346, 97, 25);
		add(backButton);

		uploadButton = new JButton("Upload");
		uploadButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		uploadButton.setBounds(339, 308, 97, 25);
		add(uploadButton);

		downloadButton = new JButton("Download");
		downloadButton.setBounds(473, 308, 97, 25);
		add(downloadButton);

		activeButton = new JButton("Activate");
		activeButton.setBounds(339, 346, 97, 25);
		add(activeButton);

		courseName = new JTextField();
		courseName.setEditable(false);
		courseName.setBackground(SystemColor.control);
		courseName.setBounds(209, 26, 162, 38);
		courseName.setBorder(null);
		add(courseName);
		courseName.setColumns(10);

		dropboxButton = new JButton("Dropbox");
		dropboxButton.setBounds(473, 33, 97, 25);
		add(dropboxButton);

	}

	/**
	 * Displays a confirm dialog for assignment activation and deactivation
	 * @param s activated or deactivated
	 */
	public void showActiveStatus(String s) {
		JOptionPane.showMessageDialog(null, "Assignment has been " + s + ". ");
	}
	
	/**
	 * Displays a confirm dialog if no assignment is selected
	 */
	public void showInvalid()
	{
		JOptionPane.showMessageDialog(null, "Please select an assignment. ");
	}
	
	/**
	 * Displays a confirm dialog if an assignment is downloaded
	 */
	public void showDownloaded()
	{
		JOptionPane.showMessageDialog(null, "File has been downloaded to the default download location.  ");
	}
	
	/**
	 * Customizes the panel for a student user
	 */
	public void changeToStudent()
	{
		remove(activeButton);
		remove(deactiveButton);
		remove(uploadButton);
	}

	/**
	 * adds an actionlistener
	 * @param list the AssignmentPanelListener
	 */
	public void addListener(AssignmentPanelListener list) {
		deactiveButton.addActionListener(list);
		activeButton.addActionListener(list);
		uploadButton.addActionListener(list);
		downloadButton.addActionListener(list);
		backButton.addActionListener(list);
		dropboxButton.addActionListener(list);
	}

	//below are the getter/setter methods
	
	public void setCourseName(String string) {
		courseName.setText(string);
		String[] temp = string.split(" ");
		courseid = temp[0];
	}

	public JButton getDeactiveButton() {
		return deactiveButton;
	}

	public JButton getActiveButton() {
		return activeButton;
	}

	public JButton getUploadButton() {
		return uploadButton;
	}

	public JButton getDropboxButton() {
		return dropboxButton;
	}

	public JButton getDownloadButton() {
		return downloadButton;
	}

	public JButton getBackButton() {
		return backButton;
	}

	public String getCourseid() {
		return courseid;
	}

	public DefaultListModel getListModel() {

		return model;
	}
	
	public JList getList()
	{
		return list;
	}
}
