package view;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;

import client.Client;
import controller.CoursePanelListener;

import javax.swing.JButton;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**
 * A custom JPanel for displaying courses
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class CoursePanel extends JPanel {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4251186208846190325L;
	private JButton studentListButton, emailButton, assignmentButton, homeButton;
	private JTextField courseName;
	private JButton activateButton;
	private JButton deactivateButton;
	
	private String name;
	private String number;
	private Client client;

	/**
	 * creates the panel
	 * @param s the number and name
	 * @param c the client
	 */
	public CoursePanel(String s, Client c) {
		client = c;
		setBackground(new Color(255, 255, 255));
		setPreferredSize(new Dimension(600, 400));
		setLayout(null);

		setStudentListButton(new JButton("Classlist"));
		getStudentListButton().addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		getStudentListButton().setBounds(113, 57, 376, 56);
		add(getStudentListButton());

		emailButton = new JButton("Email All Students");
		emailButton.setBounds(113, 115, 376, 56);
		add(emailButton);

		assignmentButton = new JButton("Assignments");
		assignmentButton.setBounds(113, 174, 376, 56);
		add(assignmentButton);

		homeButton = new JButton("Return Home");
		homeButton.setBounds(113, 291, 376, 56);
		add(homeButton);
		
		courseName = new JTextField();
		courseName.setHorizontalAlignment(SwingConstants.CENTER);
		courseName.setBackground(new Color(255, 255, 255));
		courseName.setEditable(false);
		courseName.setBounds(205, 13, 189, 31);
		courseName.setBorder(null);
		add(courseName);
		courseName.setColumns(10);
		
		activateButton = new JButton("Activate Course");
		activateButton.setBounds(113, 362, 144, 25);
		add(activateButton);
		
		deactivateButton = new JButton("Deactivate Course");
		deactivateButton.setBounds(345, 362, 144, 25);
		add(deactivateButton);
		
		setData(s);
	}
	
	/**
	 * Changes the panel to customize to student
	 */
	public void changeToStudent()
	{
		remove(activateButton);
		remove(deactivateButton);
		studentListButton.setText("Grades");
		emailButton.setText("Email Professor");
	}
	
	/**
	 * Displays a confirm dialog for course activation and deactivation
	 * @param s activated or deactivated
	 */
	public void showActiveStatus(String s)
	{
		JOptionPane.showMessageDialog(null, "Course has been " + s + ". ");
	}
	
	/**
	 * Sets name and number using the String
	 * @param s the String
	 */
	private void setData(String s)
	{
		courseName.setText(s);
		String[] temp = s.split(" ");
		number = temp[0];
		name = temp[1];
	}
	
	/**
	 * Adds a action listener
	 * @param c the CoursePanelListener
	 */
	public void addListener(CoursePanelListener c)
	{
		studentListButton.addActionListener(c);
		emailButton.addActionListener(c);
		assignmentButton.addActionListener(c);
		homeButton.addActionListener(c);
		activateButton.addActionListener(c);
		deactivateButton.addActionListener(c);
	}
	
	//below are getter methods
	public JTextField getCourseName()
	{
		return courseName;
	}
	
	public void setCourseName(String s)
	{
		courseName.setText(s);
	}

	public JButton getStudentListButton() {
		return studentListButton;
	}

	public void setStudentListButton(JButton studentListButton) {
		this.studentListButton = studentListButton;
	}
	
	public JButton getEmailButton()
	{
		return emailButton;
	}
	
	public JButton getAssignmentButton()
	{
		return assignmentButton;
	}
	
	public JButton getHomeButton()
	{
		return homeButton;
	}
	
	public JButton getActivateButton()
	{
		return activateButton;
	}
	
	public JButton getDeactivateButton()
	{
		return deactivateButton;
	}
	
	public String getName()
	{
		return name;
	}
	
	public String getNumber()
	{
		return number;
	}
}
