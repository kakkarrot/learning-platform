package view;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

import java.awt.Font;
import javax.swing.SwingConstants;

import controller.LoginListener;

import javax.swing.JPasswordField;
import java.awt.Component;
import javax.swing.Box;
import javax.swing.JButton;
/**
 * The panel used for logging in.
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class LoginGui {

	private JFrame frame;
	private JTextField textField;
	private JPasswordField passwordField;
	private JButton btnLogin;

	/**
	 * Create the application.
	 */
	public LoginGui() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 450, 300);
		frame.getContentPane().setLayout(null);

		textField = new JTextField();
		textField.setBounds(139, 94, 176, 22);
		frame.getContentPane().add(textField);
		textField.setColumns(10);

		JLabel lblUserId = new JLabel("User ID: ");
		lblUserId.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblUserId.setBounds(55, 96, 56, 16);
		frame.getContentPane().add(lblUserId);

		JLabel lblPassword = new JLabel("Password: ");
		lblPassword.setFont(new Font("Tahoma", Font.PLAIN, 14));
		lblPassword.setBounds(55, 141, 72, 16);
		frame.getContentPane().add(lblPassword);

		JLabel lblLogin = new JLabel("Learning Platform Login");
		lblLogin.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblLogin.setHorizontalAlignment(SwingConstants.CENTER);
		lblLogin.setBounds(94, 46, 271, 35);
		frame.getContentPane().add(lblLogin);

		passwordField = new JPasswordField();
		passwordField.setBounds(139, 139, 176, 22);
		frame.getContentPane().add(passwordField);

		btnLogin = new JButton("Login");
		btnLogin.setBounds(177, 174, 97, 25);
		frame.getContentPane().add(btnLogin);
		
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
	}
	
	/**
	 * Error message for when the login credentials are incorrect.
	 */
	public void showInvalid()
	{
		JOptionPane.showMessageDialog(null, "Invalid Login Info. ");
	}
	
	//Getter functions are below
	public JButton getLogin()
	{
		return btnLogin;
	}
	
	public JFrame getFrame()
	{
		return frame;
	}
	
	public JTextField get_id()
	{
		return textField;
	}
	
	public JPasswordField getPass()
	{
		return passwordField;
	}
	
	public static void main(String[] args)
	{
		LoginGui a = new LoginGui();
	}
}
