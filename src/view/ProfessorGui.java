package view;

import java.awt.EventQueue;
import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;

import client.Client;
import controller.HomeListener;
import shareddataclasses.Course;
import shareddataclasses.Message;
/**
 * The GUI for the professor.
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class ProfessorGui extends View {

	/**
	 * Create the application.
	 */
	public ProfessorGui(Message <Vector<Course>> m, Client c) {
		client = c;
		initialize();
		createCourseList(m.getContent());
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		home = new Home(client);
		home.addListener(new HomeListener(home, client));

		frame = new JFrame();
		frame.setBounds(100, 100, 598, 459);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.getContentPane().add(home);

		frame.setVisible(true);
	}
	
	/**
	 * Adds a list of the courses the professor teaches to the panel.
	 * @param v
	 */
	public void createCourseList(Vector<Course> v)
	{
		for (int i = 0; i < v.size(); i++)
		{
			getHome().getCourseList().addItem(v.get(i).getNumber() + " " + v.get(i).getName());
		}
		DefaultComboBoxModel<String> model = createModel(v);
		home.getCourseList().setModel(model);
	}
	
	/**
	 * Helper function to add courses to the panel.
	 * @param v
	 * @return
	 */
	public DefaultComboBoxModel<String> createModel(Vector<Course> v)
	{
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
		for (int i = 0; i < v.size(); i++)
		{
			model.addElement(v.get(i).getNumber() + " " + v.get(i).getName());
		}
		return model;
	}

}
