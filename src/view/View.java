package view;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.util.Vector;

import javax.swing.JFrame;
import javax.swing.JPanel;

import client.Client;
import controller.LoginListener;
/**The main class that has JPanel properties.
 * 
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class View{
	
	/**
	 * The main frame.
	 */
	protected JFrame frame;
	/**
	 * The main panel for home.
	 */
	protected Home home;
	/**
	 * The main panel to display courses.
	 */
	protected CoursePanel course;
	/**
	 * The client that uses this view.
	 */
	protected Client client;
	/**
	 * The panel used to search for students.
	 */
	protected SearchStudentPanel searchResultsPanel;
	/**
	 * The panel used to search for assignments.
	 */
	protected AssignmentPanel assignmentPanel;
	/**
	 * The panel used to add assignments.
	 */
	protected AddAssignmentPanel addAssignmentPanel;
	/**
	 * The panel used to facilitate the drop box.
	 */
	protected DropboxPanel dropboxPanel;
	/**
	 * The panel used to display submission grades and grade submissions.
	 */
	protected GradesPanel gradesPanel;
	
//	public View()
//	{
//		home = new Home();
//		course = new CoursePanel();
//		searchResultsPanel = new SearchStudentPanel();
//		assignmentPanel = new AssignmentPanel();
//		addAssignmentPanel = new AddAssignmentPanel();
//		initialize();
//	}
	
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {

		frame = new JFrame();
		frame.setBounds(100, 100, 640, 486);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.setVisible(true);
	}
	
	
	
	/**
	 * Removes login from frame.
	 */
	public void clear()
	{
		frame.getContentPane().removeAll();
		frame.getContentPane().repaint();
		frame.getContentPane().revalidate();
	}
	
	/**
	 * Displays the GradesPanel.
	 * @param s
	 */
	public void displayGradesPanel(String s)
	{
		deletePanel(gradesPanel);
		gradesPanel = new GradesPanel(s, client);
		finalize(gradesPanel);
	}
	
	/**
	 * Brings the home JPanel to the JFrame
	 */
	public void displayAddAssignmentPanel(String s)
	{
		deletePanel(addAssignmentPanel);
		addAssignmentPanel = new AddAssignmentPanel(s, client);	
		finalize(addAssignmentPanel);
	}
	
	/**
	 * Displays the home panel.
	 */
	public void displayHome()
	{		
		finalize(home);
	}
	
	/**
	 * Displays the class list on the search panel.
	 * @param s
	 */
	public void displayClassList(String s)
	{
		deletePanel(searchResultsPanel);
		searchResultsPanel = new SearchStudentPanel(s, client);	
		finalize(searchResultsPanel);
	}
	
	/**
	 * Displays the course panel.
	 * @param s
	 */
	public void displayCoursePanel(String s)
	{
		deletePanel(course);
		course = new CoursePanel(s, client);	
		finalize(course);
	}
	
	/**
	 * Displays a list of assignments.
	 * @param s
	 */
	public void displayAssignementList(String s) {
		deletePanel(assignmentPanel);
		assignmentPanel = new AssignmentPanel(s, client);	
		finalize(assignmentPanel);
	}
	
	/**
	 * Displays the panel for the drop box.
	 */
	public void displayDropboxPanel(String s)
	{
		deletePanel(dropboxPanel);
		dropboxPanel = new DropboxPanel(s, client);	
		finalize(dropboxPanel);
	}
	
	/**
	 * Deletes a panel.
	 * @param a
	 * 		The panel to be deleted.
	 */
	private void deletePanel(JPanel a)
	{
		if (a == null)
			return;
		a.removeAll();
		a.setVisible(false);
		a.setEnabled(false);
	}
	
	/**
	 * Adds and repaints a JPanel, refreshes and displays it.
	 */
	private void finalize(JPanel a)
	{
		a.setVisible(true);
		clear();
		frame.getContentPane().add(a);
		frame.getContentPane().repaint();
		frame.getContentPane().revalidate();
		frame.getContentPane().setVisible(false);
		frame.getContentPane().setVisible(true);
		frame.setVisible(false);
		frame.setVisible(true);
	}
	
	//below are the getter methods
	public Home getHome()
	{
		return home;
	}
	
	public JFrame getFrame()
	{
		return frame;
	}
	
	public CoursePanel getCoursePanel()
	{
		return course;
	}
	
	public SearchStudentPanel getSearchResultsPanel()
	{
		return searchResultsPanel;
	}
	
	public AssignmentPanel getAssignmentPanel()
	{
		return assignmentPanel;
	}
	
	public AddAssignmentPanel getAddAssignmentPanel()
	{
		return addAssignmentPanel;
	}
	
	public GradesPanel getGradesPanel()
	{
		return gradesPanel;
	}
	
	public DropboxPanel getDropboxPanel()
	{
		return dropboxPanel;
	}
	
	public Client getClient()
	{
		return client;
	}

	
}
