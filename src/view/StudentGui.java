package view;

import java.util.Vector;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JFrame;

import client.Client;
import controller.HomeListener;
import shareddataclasses.Course;
import shareddataclasses.Message;

public class StudentGui extends View{
	
	/**
	 * Create the application.
	 */
	public StudentGui(Message <Vector<Course>> m, Client c) {
		client = c;
		initialize();
		createCourseList(m.getContent());
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		home = new Home(client);
		home.addListener(new HomeListener(home, client));
		home.remove(home.getCreate());

		frame = new JFrame();
		frame.setBounds(100, 100, 598, 459);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		frame.getContentPane().add(home);

		frame.setVisible(true);
	}
	
	/**
	 * Adds a vector of courses to the panel.
	 * @param v
	 */
	public void createCourseList(Vector<Course> v)
	{
		for (int i = 0; i < v.size(); i++)
		{
			getHome().getCourseList().addItem(v.get(i).getNumber() + " " + v.get(i).getName());
		}
		DefaultComboBoxModel<String> model = createModel(v);
		home.getCourseList().setModel(model);
	}
	
	/**
	 * Creates the model to be used for displaying the courses.
	 * @param v
	 * @return
	 */
	public DefaultComboBoxModel<String> createModel(Vector<Course> v)
	{
		DefaultComboBoxModel<String> model = new DefaultComboBoxModel<String>();
		for (int i = 0; i < v.size(); i++)
		{
			model.addElement(v.get(i).getNumber() + " " + v.get(i).getName());
		}
		return model;
	}
}
