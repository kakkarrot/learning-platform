package view;

import java.awt.Dimension;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import client.Client;
import controller.DropboxPanelListener;

import javax.swing.SwingConstants;
import java.awt.Font;

/**
 * The class for submitting assignments.
 * @author Erik Skoronski
 * @author Qifeng Li
 *
 */
public class DropboxPanel extends JPanel {
	
	private DefaultListModel<String> model;
	private JScrollPane submissions;
	private JTextArea searchResults;
	private JList<String> listArea;
	private JList list;
	private JButton backButton;
	private JButton uploadButton;
	private JButton downloadButton;
	private JButton gradeButton;
	private String assignmentid;
	private Client client;

	/**
	 * Creates the panel.
	 */
	public DropboxPanel(String s, Client c) {
		assignmentid = s;
		client = c;
		setPreferredSize(new Dimension(724, 600));
		setLayout(null);

		model = new DefaultListModel<String>();
		listArea = new JList<String>(model);
		listArea.setPrototypeCellValue("123456789012345678901234567890");
		listArea.setVisibleRowCount(10);
		searchResults = new JTextArea(100, 100);
		submissions = new JScrollPane(listArea);

		list = new JList(model);
		list.setBounds(29, 76, 541, 225);
		add(list);

		backButton = new JButton("Home");
		backButton.setBounds(42, 346, 97, 25);
		add(backButton);

		uploadButton = new JButton("Upload");
		uploadButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		uploadButton.setBounds(339, 308, 97, 25);
		add(uploadButton);

		downloadButton = new JButton("Download");
		downloadButton.setBounds(473, 308, 97, 25);
		add(downloadButton);

		JTextField submissionsText = new JTextField();
		submissionsText.setFont(new Font("Tahoma", Font.PLAIN, 18));
		submissionsText.setHorizontalAlignment(SwingConstants.CENTER);
		submissionsText.setText("Submissions");
		submissionsText.setEditable(false);
		submissionsText.setBackground(SystemColor.control);
		submissionsText.setBounds(209, 26, 162, 38);
		submissionsText.setBorder(null);
		add(submissionsText);
		submissionsText.setColumns(10);
		
		gradeButton = new JButton("Grade");
		gradeButton.setBounds(473, 346, 97, 25);
		add(gradeButton);
	}
	
	/**
	 * Error message for when no submission is selected.
	 */
	public void showInvalid()
	{
		JOptionPane.showMessageDialog(null, "Please select a submission. ");
	}
	
	/**
	 * Displays message when an item is successfully uploaded.
	 */
	public void showUploaded()
	{
		JOptionPane.showMessageDialog(null, "Assignment submitted. ");
	}
	
	/**
	 * Displays message when a submission has been graded.
	 */
	public void showGraded()
	{
		JOptionPane.showMessageDialog(null, "Grade submitted. ");
	}
	
	/**
	 * Displays message when an item is successfully downloaded.
	 */
	public void showDownloaded()
	{
		JOptionPane.showMessageDialog(null, "Submission has been downloaded to the default location. ");
	}
	
	/**
	 * Adds listener to the panel.
	 * @param l
	 */
	public void addListener(DropboxPanelListener l)
	{
		backButton.addActionListener(l);
		gradeButton.addActionListener(l);
		downloadButton.addActionListener(l);
		uploadButton.addActionListener(l);
	}
	
	/**
	 * Mode selection for Professor.
	 */
	public void changeToProfessor()
	{
		remove(uploadButton);
	}
	
	/**
	 * Mode selection for Student.
	 */
	public void changeToStudent()
	{
		remove(gradeButton);
	}
	
	//below are the getter methods
	
	public JButton getUploadButton()
	{
		return uploadButton;
	}
	
	public JButton getBackButton()
	{
		return backButton;
	}
	
	public JButton getDownloadButton()
	{
		return downloadButton;
	}
	
	public JButton getGradeButton()
	{
		return gradeButton;
	}
	
	public DefaultListModel getListModel() {

		return model;
	}
	
	public String getAssignmentid()
	{
		return assignmentid;
	}
	
	public JList getList()
	{
		return list;
	}
	
}
