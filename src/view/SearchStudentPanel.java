package view;

import java.awt.Color;
import java.awt.Dimension;

import javax.swing.ButtonGroup;
import javax.swing.DefaultListModel;
import javax.swing.JComboBox;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;

import client.Client;
import controller.SearchStudentPanelListener;

import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
/**The panel used for searching for and diplaying students.
 * 
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class SearchStudentPanel extends JPanel {

	private static final long serialVersionUID = -6941861264789478425L;

	private JTextField searchLabel;
	private JRadioButton idRadioButton, lastNameRadioButton;
	private ButtonGroup radioGroup;
	private DefaultListModel<String> model;
	private JScrollPane courses;
	private JTextArea searchResults;
	private JList<String> listArea;
	private JList list;
	private JTextField selectedTextField;
	private JButton searchButton;
	private JTextField queryField;
	String[] results;
	private JButton backButton;
	private JButton enrollButton;
	private JButton unenrollButton;
	private String courseid;
	private Client client;
	
	/**
	 * Create the panel.
	 */
	public SearchStudentPanel(String s, Client c) {
		courseid = s;
		client = c;
		setBackground(Color.PINK);
		setPreferredSize(new Dimension(724, 600));
		setLayout(null);
		
		searchLabel = new JTextField();
		searchLabel.setHorizontalAlignment(SwingConstants.CENTER);
		searchLabel.setText("Search By: ");
		searchLabel.setBounds(23, 22, 116, 22);
		searchLabel.setBorder(null);
		add(searchLabel);
		searchLabel.setColumns(10);
		
		radioGroup = new ButtonGroup();
		idRadioButton = new JRadioButton("ID");
		idRadioButton.setBackground(Color.WHITE);
		idRadioButton.setHorizontalAlignment(SwingConstants.CENTER);
		idRadioButton.setBounds(191, 21, 127, 25);
		idRadioButton.setBorder(null);
		radioGroup.add(idRadioButton);
		add(idRadioButton);
		
		lastNameRadioButton = new JRadioButton("Last Name");
		lastNameRadioButton.setBackground(Color.WHITE);
		lastNameRadioButton.setHorizontalAlignment(SwingConstants.CENTER);
		lastNameRadioButton.setBounds(362, 21, 127, 25);
		lastNameRadioButton.setBorder(null);
		radioGroup.add(lastNameRadioButton);
		add(lastNameRadioButton);
		
		model = new DefaultListModel<String>();
		listArea = new JList<String>(model);
		listArea.setPrototypeCellValue("123456789012345678901234567890");
		listArea.setVisibleRowCount(10);
		searchResults = new JTextArea(100,100);
		courses = new JScrollPane(listArea);
		
		list = new JList(model);
		list.setBounds(29, 76, 541, 225);
		add(list);
		
		searchButton = new JButton("Search");
		searchButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		searchButton.setBounds(473, 346, 97, 25);
		add(searchButton);
		
		queryField = new JTextField();
		queryField.setBounds(233, 347, 203, 22);
		add(queryField);
		queryField.setColumns(10);
		
		backButton = new JButton("Home");
		backButton.setBounds(42, 346, 97, 25);
		add(backButton);
		
		enrollButton = new JButton("Enroll");
		enrollButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		enrollButton.setBounds(339, 308, 97, 25);
		add(enrollButton);
		
		unenrollButton = new JButton("Unenroll");
		unenrollButton.setBounds(473, 308, 97, 25);
		add(unenrollButton);
	}
	
	/**
	 * Adds Listener to the panel.
	 * @param listener
	 */
	public void addListener(SearchStudentPanelListener listener)
	{
		backButton.addActionListener(listener);
		searchButton.addActionListener(listener);
		enrollButton.addActionListener(listener);
		unenrollButton.addActionListener(listener);
	}
	
	/**
	 * Shoes error message when no search mode is selected.
	 */
	public void showInvalidSearch()
	{
		JOptionPane.showMessageDialog(null, "Please select search criteria. ");
	}
	
	/**
	 * Shoes error message when no student is selected.
	 */
	public void showInvalidEnrollment() {
		JOptionPane.showMessageDialog(null, "Please select a student to change enrollment. ");
	}
	
	public JButton getSearchButton()
	{
		return searchButton;
	}
	
	public JTextField getQueryField()
	{
		return queryField;
	}
	
	public JRadioButton getIDRadioButton()
	{
		return idRadioButton;
	}
	
	public JRadioButton getLastNameRadioButton()
	{
		return lastNameRadioButton;
	}
	
	public JList getJList()
	{
		return list;
	}
	
	public DefaultListModel getListModel()
	{

		return model;
	}
	
	public JButton getBackButton()
	{
		return backButton;
	}

	public JButton getEnrollButton() {
		return enrollButton;
	}
	
	public JButton getUnenrollButton() {
		return unenrollButton;
	}

	public String getCourseid() {
		return courseid;
		
	}
	public void setCourseid(String id) {
		courseid = id;
		
	}

}
