package view;

import javax.swing.JPanel;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;

import client.Client;
import controller.HomeListener;

import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JComboBox;
/**
 * The view displayed after logging in, depends on whether a professor or a student has logged in.
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class Home extends JPanel {


	private static final long serialVersionUID = -8446018937985438480L;
	
	private Client client;
	private JButton continueButton, createCourseButton;
	private JComboBox<String> courseList;

	/**
	 * Creates the home panel.
	 * @param c
	 */
	public Home(Client c) {
		client = c;
		setBackground(Color.LIGHT_GRAY);
		setPreferredSize(new Dimension(600, 400));
		setLayout(null);
		
		createCourseButton = new JButton("Create new Course");
		createCourseButton.setBounds(431, 308, 143, 25);
		add(createCourseButton);
		
		courseList = new JComboBox<String>();
		courseList.setBounds(100, 109, 402, 22);
		add(courseList);
		
		JTextField instructionText = new JTextField();
		instructionText.setText("Please select a course. ");
		instructionText.setEditable(false);
		instructionText.setBackground(Color.LIGHT_GRAY);
		instructionText.setBounds(100, 74, 197, 22);
		instructionText.setBorder(null);
		add(instructionText);
		instructionText.setColumns(10);

		JTextField welcomeText = new JTextField();
		welcomeText.setEditable(false);
		welcomeText.setFont(new Font("Tahoma", Font.PLAIN, 20));
		welcomeText.setBackground(Color.LIGHT_GRAY);
		welcomeText.setHorizontalAlignment(SwingConstants.CENTER);
		welcomeText.setText("Welcome! ");
		welcomeText.setBounds(12, 13, 118, 31);
		add(welcomeText);
		welcomeText.setColumns(10);
		welcomeText.setBorder(null);
		
		continueButton = new JButton("Continue");
		continueButton.setBounds(477, 270, 97, 25);
		add(continueButton);
	}
	
	/**
	 * Adds a listener to the Home panel.
	 * @param h
	 */
	public void addListener(HomeListener h)
	{
		continueButton.addActionListener(h);
		createCourseButton.addActionListener(h);
	}
	
	//below are the getter methods
	
	public JComboBox<String> getCourseList()
	{
		return courseList;
	}
	
	public JButton getContinue()
	{
		return continueButton;
	}
	
	public JButton getCreate()
	{
		return createCourseButton;
	}
}
