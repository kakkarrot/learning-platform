package view;

import java.awt.Dimension;
import java.awt.SystemColor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import client.Client;
import controller.AssignmentPanelListener;
import controller.GradesPanelListener;

import javax.swing.JLabel;
import javax.swing.SwingConstants;
import java.awt.Font;

/**
 * The panel that displays the grades for a student.
 * @author Erik Skoronski
 * @author Qifeng Li
 *
 */
public class GradesPanel extends JPanel {

	private DefaultListModel<String> model;
	private JScrollPane grades;
	private JTextArea searchResults;
	private JList<String> listArea;
	private JList list;
	private JButton backButton;
	private JTextField gradesLabel;
	private Client client;
	private String studentid;

	/**
	 * Creates the panel.
	 */
	public GradesPanel(String s, Client c) {
		studentid = s;
		client = c;
		setPreferredSize(new Dimension(724, 600));
		setLayout(null);

		model = new DefaultListModel<String>();
		listArea = new JList<String>(model);
		listArea.setPrototypeCellValue("123456789012345678901234567890");
		listArea.setVisibleRowCount(10);
		searchResults = new JTextArea(100, 100);
		grades = new JScrollPane(listArea);

		gradesLabel = new JTextField();
		gradesLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		gradesLabel.setHorizontalAlignment(SwingConstants.CENTER);
		gradesLabel.setText("Grades");
		gradesLabel.setEditable(false);
		gradesLabel.setBackground(SystemColor.control);
		gradesLabel.setBounds(233, 25, 162, 38);
		gradesLabel.setBorder(null);
		add(gradesLabel);
		gradesLabel.setColumns(10);

		list = new JList(model);
		list.setBounds(29, 76, 541, 225);
		add(list);

		backButton = new JButton("Home");
		backButton.setBounds(42, 346, 97, 25);
		add(backButton);

	}

	/**
	 * Adds a listener to the panel.
	 * @param l
	 */
	public void addListener(GradesPanelListener l)
	{
		backButton.addActionListener(l);
	}

	public JButton getBackButton() {
		return backButton;
	}

	public String getStudentid() {
		return studentid;
	}

	public DefaultListModel getListModel() {

		return model;
	}

	public JList getList() {
		return list;
	}
}
