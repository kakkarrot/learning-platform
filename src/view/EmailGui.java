package view;

import java.awt.Dimension;

import javax.swing.DefaultListModel;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import controller.EmailListener;

/**
 * Panel used for sending emails to either the professor or the class of students.
 * @author Erik Skoronski
 * @author Qifeng Li
 *
 */
public class EmailGui extends JFrame {
	private JTextField subjectText;
	private JTextArea emailText;
	private JButton sendButton, cancelButton;
	private String courseid;
	
	
	/**
	 * Creates the panel.
	 */
	public EmailGui(String s) {
		courseid = s;
		setSize(600, 400);
		getContentPane().setLayout(null);
		
		emailText = new JTextArea();
		JScrollPane scrollPane = new JScrollPane(emailText);
		scrollPane.setBounds(88, 45, 482, 259);
		getContentPane().add(scrollPane);
		
		sendButton = new JButton("Send");
		sendButton.setBounds(473, 317, 97, 25);
		getContentPane().add(sendButton);
		
		cancelButton = new JButton("Cancel");
		cancelButton.setBounds(359, 317, 97, 25);
		getContentPane().add(cancelButton);
		
		subjectText = new JTextField();
		subjectText.setBounds(88, 10, 482, 22);
		getContentPane().add(subjectText);
		subjectText.setColumns(10);
		
		JLabel lblSubject = new JLabel("Subject:");
		lblSubject.setHorizontalAlignment(SwingConstants.CENTER);
		lblSubject.setBounds(12, 13, 64, 16);
		getContentPane().add(lblSubject);
		
		JLabel lblBody = new JLabel("Body: ");
		lblBody.setHorizontalAlignment(SwingConstants.CENTER);
		lblBody.setBounds(12, 49, 64, 16);
		getContentPane().add(lblBody);
		
		setVisible(true);
	}
	
	/**
	 * Adds a listener to the panel.
	 * @param l
	 */
	public void addListener(EmailListener l)
	{
		sendButton.addActionListener(l);
		cancelButton.addActionListener(l);
	}
	
	/**
	 * Error message for when nothing is in the fields to enter email content.
	 */
	public void showInvalid()
	{
		JOptionPane.showMessageDialog(null, "Subject and body fields must both contain text. ");
	}
	
	/**
	 * Shows that an email has been sent.
	 */
	public void showSent()
	{
		JOptionPane.showMessageDialog(null, "Email(s) have been sent. ");
	}
	
	//below are the getter methods
	public JTextField getSubject()
	{
		return subjectText;
	}
	
	public JTextArea getBody()
	{
		return emailText;
	}
	
	public JButton getSend()
	{
		return sendButton;
	}
	
	public JButton getCancel()
	{
		return cancelButton;
	}
	
	public String getCourseid()
	{
		return courseid;
	}
}
