package view;

import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import javax.swing.SwingConstants;

import java.awt.Dimension;
import java.awt.Font;
/**
 * A custom JPanel for adding courses
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class AddCoursePanel extends JPanel {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3169128995283929797L;
	private JTextField course_id;
	private JTextField courseName;

	/**
	 * Create the panel.
	 */
	public AddCoursePanel() {
		JLabel lblCourseId = new JLabel("Course ID:");
		add(lblCourseId);
		
		course_id = new JTextField();
		add(course_id);
		
		course_id.setColumns(10);
		JLabel lblCourseName = new JLabel("Course Name:");
		add(lblCourseName);
		
		courseName = new JTextField();
		add(courseName);
		courseName.setColumns(10);
		
		setVisible(true);

	}
	
	/**
	 * Checks to see if the contents in the course are not null
	 * @return true if all fields full
	 */
	public boolean contentsGood()
	{
		if(isNull(course_id) || isNull(courseName))
		{
			showInvalid();
			return false;
		}
		return true;
	}
	
	/**
	 * Checks to see if a JTextField is null
	 * @param a the JTextField
	 * @return True if a is null
	 */
	private boolean isNull(JTextField a)
	{
		if (a == null || a.getText().equals(""))
			return true;
		return false;
	}
	
	/**
	 * Creates confirm dialog if error occurs
	 */
	public void showInvalid()
	{
		JOptionPane.showMessageDialog(null, "Course number and name cannot be empty. ");
	}
	
	//below are the getter methods
	
	public JTextField getCourse_id()
	{
		return course_id;
	}
	
	public JTextField getCourseName()
	{
		return courseName;
	}
}
