package view;

import java.awt.Dimension;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.SwingConstants;

import client.Client;
import controller.AddAssignmentPanelListener;

import javax.swing.JTextField;
import javax.swing.JButton;
/**
 * A custom JPanel for adding assignments
 * @author Erik Skoronski
 * @author Qifeng Li
 */
public class AddAssignmentPanel extends JPanel {
	private JTextField assignmentidText;
	private JTextField titleText;
	private JTextField dueDateText;
	private JButton chooseFileButton, confirmButton, homeButton;
	private String courseid;
	private Client client;

	/**
	 * Create the panel.
	 */
	public AddAssignmentPanel(String s, Client c) {
		courseid = s;
		client = c;
		setPreferredSize(new Dimension(600, 400));
		setLayout(null);
		
		JLabel lblUploadNewAssignment = new JLabel("Upload New Assignment");
		lblUploadNewAssignment.setHorizontalAlignment(SwingConstants.CENTER);
		lblUploadNewAssignment.setBounds(204, 13, 165, 16);
		add(lblUploadNewAssignment);
		
		JLabel lblAssignmentid = new JLabel("AssignmentID:");
		lblAssignmentid.setBounds(123, 42, 96, 16);
		add(lblAssignmentid);
		
		assignmentidText = new JTextField();
		assignmentidText.setBounds(273, 39, 116, 22);
		add(assignmentidText);
		assignmentidText.setColumns(10);
		
		JLabel lblTitle = new JLabel("Title:");
		lblTitle.setBounds(123, 81, 96, 16);
		add(lblTitle);
		
		titleText = new JTextField();
		titleText.setColumns(10);
		titleText.setBounds(273, 78, 116, 22);
		add(titleText);
		
		JLabel lblDueDate = new JLabel("Due Date:");
		lblDueDate.setBounds(123, 125, 96, 16);
		add(lblDueDate);
		
		dueDateText = new JTextField();
		dueDateText.setColumns(10);
		dueDateText.setBounds(273, 122, 116, 22);
		add(dueDateText);
		
		chooseFileButton = new JButton("Choose File");
		chooseFileButton.setBounds(204, 176, 110, 25);
		add(chooseFileButton);
		
		confirmButton = new JButton("Confirm");
		confirmButton.setBounds(347, 176, 110, 25);
		add(confirmButton);
		
		homeButton = new JButton("Home");
		homeButton.setBounds(59, 176, 110, 25);
		add(homeButton);
		
	}
	
	/**
	 * adds an action listener
	 * @param a the AddAssignmentPanelListener
	 */
	public void addListener(AddAssignmentPanelListener a)
	{
		chooseFileButton.addActionListener(a);
		confirmButton.addActionListener(a);
		homeButton.addActionListener(a);
	}
	
	
	
	//below are the getter methods
	public JButton getChooseFileButton()
	{
		return chooseFileButton;
	}
	
	public JButton getConfirmButton()
	{
		return confirmButton;
	}
	
	public JButton getHomeButton()
	{
		return homeButton;
	}
	
	public String getCourseid() {
		return courseid;
	}

	public void setCourseid(String c) {
		courseid = c;
	}
	
	public JTextField getAssignmentidText()
	{
		return assignmentidText;
	}
	
	public JTextField getTitleText()
	{
		return titleText;
	}
	
	public JTextField getDueDateText()
	{
		return dueDateText;
	}
	
	

}
